# README #

Information system of bank for clients and employee.

### About project ###

* PHP 5.3.1, Nette Framework, HTML, CSS, Bootstrap, 
* school project for IPP - VUT FIT
* DO NOT COPY - ONLY FOR INSPIRATION

### Presenters ###
* CommonPresenter.php - základní presenter, načítání obrázků.
* BackendPresenter.php - rozšiřuje funkcionalitu Common a kontroluje pokaždé zda je uživatel přihlášen při přístupu do backendu.
* FrontendPresenter.php - rozšiřuje funkcionalitu Common a kontroluje pokaždé zda je uživatel přihlášen při přístupu do frontendu.
* HomepagePresenter.php - formuláře, změna hesla, render funkce pro klienta
* AdminPresenter.php - rendery pro administrátora a zaměstnance, formuláře přidání a editace klienta, zaměstnance, účtů, poboček.
* ErrorPresenter.php - výpis chyb
* SignPresenter.php - formulář pro přihlášení

### Modely ###
* AccessManager.php - přídávání, mazání, editace, hledání v databázové tabulce disponentů.
* BranchManager.php - přídávání, mazání, editace, hledání v databázové tabulce pobočky.
* ClientManager.php - přídávání, mazání, editace, hledání v databázové tabulce klienti.
* EmployeeManager.php - přídávání, mazání, editace, hledání v databázové tabulce pracovník banky.
* TransactionManager.php - přídávání, mazání, editace, hledání v databázové tabulce operace.
* UserManager.php - autorizace, autentifikace, změna hesel, hashování hesla.
* Settings.php - převod na ASCII znaky.

### Instalation ###
1. The content of "bank" inserted into the root directory of the server.
2. Import file database.sql to database on server.

### Author ###
* Adam Konecny - frontend, backend
* Tomas Hynek - GUI development, database, forms
* https://bitbucket.org/strajky/