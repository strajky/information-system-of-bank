<?php

namespace App\Presenters;

use Nette;


class BackendPresenter extends CommonPresenter
{
    public function beforeRender()
    {
        if(!$this->user->isLoggedIn())
            $this->redirect('Sign:in');

        if ($this->user->getIdentity()->role == "client") {
            $this->redirect('Admin:');
        }

        if ($this->isAjax()) {
            $this->invalidateControl('flashes');
            //$this->invalidateControl('content');
        }
    }

    public function beforeValidation() {
        if(!$this->user->isLoggedIn())
            $this->redirect('Sign:in');
    }
}
