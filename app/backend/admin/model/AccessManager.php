<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class AccessManager extends Nette\Object {
	const
		TABLE_NAME = 'access',
		COLUMN_CLIENT_ID = 'client_id',
		COLUMN_ACCOUNT_ID = 'account_id',
		COLUMN_LIMIT = 'limit';

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function addAccessToClient($clientID, $accountID, $limit) {
		try {
			$this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_CLIENT_ID => $clientID,
				self::COLUMN_ACCOUNT_ID => $accountID,
				self::COLUMN_LIMIT => $limit
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function getAccessByAccountID($accountID) {
		$access = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ACCOUNT_ID, $accountID);

		return $access;
	}

	public function getAccessWithID($accessID) {
		return $this->database->table(self::TABLE_NAME)->get($accessID);
	}

	public function getAccessAllByClientID($clientID) {
		$access = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_CLIENT_ID, $clientID);

		return $access;
	}

	public function getAccessByClientID($clientID, $accountID) {
		$access = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_CLIENT_ID, $clientID)->where(self::COLUMN_ACCOUNT_ID, $accountID);

		return $access->fetch();
	}

	public function deleteAccess($accessID) {
		try {
			$access = $this->getAccessWithID($accessID);
			$access->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}
}