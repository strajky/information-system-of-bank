<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class BranchManager extends Nette\Object {
	const
		TABLE_NAME = 'branch',
		COLUMN_ID = 'id',
		COLUMN_ADRESS = 'adress',
		COLUMN_CITY = 'city',
		COLUMN_ZIP = 'zip',
		COLUMN_ATM = 'atm',
		COLUMN_EMAIL = 'description',
		COLUMN_BRANCH = 'manager';


	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function addNewBranch($adress, $city, $zip, $atm, $description) {
		try {
			$row = $this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_ATM => $atm,
				self::COLUMN_EMAIL => $description,
			));

			return $row->id;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}

		return NULL;
	}

	public function updateBranch($id, $adress, $city, $zip, $atm, $description) {
		try {
			$branch = $this->getBranchWithID($id);

			$branch->update(array(
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_ATM => $atm,
				self::COLUMN_EMAIL => $description,
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function deleteBranch($id) {
		try {
			$branch = $this->getBranchWithID($id);
			$branch->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function getBranchWithID($branchID) {
		return $this->database->table(self::TABLE_NAME)->get($branchID);
	}

	public function getAllBranches() {
		return $this->database->table(self::TABLE_NAME)->fetchAll();
	}
}