<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use App\Model\AccessManager;

/**
 * Users management.
 */
class ClientManager extends Nette\Object {
	const
		TABLE_NAME = 'clients',
		COLUMN_ID = 'id',
		COLUMN_USERNAME = 'username',
		COLUMN_ROLE = 'role',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_PERSONAL_NUMBER = 'personal_number',
		COLUMN_PERSONAL_ID = 'personal_id',
		COLUMN_NAME = 'name',
		COLUMN_SURNAME = 'surname',
		COLUMN_ADRESS = 'adress',
		COLUMN_CITY = 'city',
		COLUMN_ZIP = 'zip',
		COLUMN_PHONE = 'phone',
		COLUMN_EMAIL = 'email',
		COLUMN_HAS_IMAGE = 'has_image',
		ACCOUNT_TABLE_NAME = 'account',
		ACCOUNT_BALANCE = 'balance',
		ACCOUNT_DATE_CREATED = 'date_created',
		ACCOUNT_OWNER = 'owner',
		ACCOUNT_NAME = 'name',
		ACCOUNT_TYPE_ID = 'type_id',
		ACCOUNT_TYPE = 'type',
		ACCOUNT_AMOUNT = 'amount',
		ACCOUNT_INTEREST = 'interest',
		ACCOUNT_DAILY_LIMIT = 'daily_limit',
		REGULAR_ACCOUNT_TABLE_NAME = 'regular_account',
		SAVINGS_ACCOUNT_TABLE_NAME = 'savings_account',
		CREDIT_ACCOUNT_TABLE_NAME = 'credit_account',
		COLUMN_BIRTHDAY_NUMBER = 'birthday_number',
		COLUMN_SEX = 'sex';

	/** @var Nette\Database\Context */
	private $database;

	public $accessManager;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getClientWithID($clientID) {
		return $this->database->table(self::TABLE_NAME)->get($clientID);
	}

	public function generatePassword($length) {
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);

	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }

	    return $result;
	}

	public function generateUsername($name, $surname) {
		$name = strtolower(Settings::convertToASCII($name));
		$surname = strtolower(Settings::convertToASCII($surname));

		$surnamePart = substr($surname, 0, 5);
		$namePart = "";

		if (strlen($surnamePart) < 5) {
			$charactersNeeded = 5 - strlen($surnamePart);

			$namePart = substr($name, 0, $charactersNeeded);
		}

		$newUsername = "x" . $surnamePart . $namePart;

		$users = $this->database->query('SELECT * FROM ' . self::TABLE_NAME . ' WHERE `' . self::COLUMN_USERNAME . '` LIKE \'%' . $newUsername . '%\'');

		$allUsers = $users->fetchAll();

		if (count($allUsers) > 0) {
			$last = end($allUsers);
			$lastUsername = $last->username;
			$lastNumber = substr($lastUsername, -2, 2);
			$lastNumber = intval($lastNumber);
			$lastNumber++;

			if ($lastNumber < 10) {
				$newUsername .= "0";
				$newUsername .= $lastNumber;
			} else {
				$newUsername .= $lastNumber;
			}
		} else {
			$newUsername .= "01";
		}

		return $newUsername;
	}

	public function changePassword($id, $oldPassword, $newPassword) {
		$client = $this->getClientWithID($id);

		try {
			if (!Passwords::verify($oldPassword, $client[self::COLUMN_PASSWORD_HASH])) {
				throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

				return 1;
			} else {
				$client->update(array(
					self::COLUMN_PASSWORD_HASH => Passwords::hash($newPassword),
				));
			}
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');

				return 2;
		}

		return 0;
	}

	public function addNewClient($name, $surname, $personal_number, $personal_id, $adress, $city, $zip, $phone, $email, $birthday_number, $sex) {
		$password = $this::generatePassword(8);
		$username = $this::generateUsername($name, $surname);

		try {
			$mail = new Message;
			$mail->setFrom('adam.konecny94@gmail.com')
				->addTo($email)
				->setSubject('Přihlašovací údaje')
				->setBody('Dobrý den, byly Vám vygenerovány přihlašovací údaje do systému. Uživatelské jméno je \'' . $username . '\' a heslo je \'' . $password . '\'');

			$mailer = new SendmailMailer;

			$row = $this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_USERNAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_PERSONAL_NUMBER => $personal_number,
				self::COLUMN_PERSONAL_ID => $personal_id,
				self::COLUMN_NAME => $name,
				self::COLUMN_SURNAME => $surname,
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_PHONE => $phone,
				self::COLUMN_EMAIL => $email,
				self::COLUMN_ROLE => "client",
				self::COLUMN_BIRTHDAY_NUMBER => $birthday_number,
				self::COLUMN_SEX => $sex
			));

			$mailer->send($mail);

			return $row->id;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function updateClient($id, $name, $surname, $personal_number, $personal_id, $adress, $city, $zip, $phone, $email, $birthday_number, $sex) {
		try {
			$client = $this->getClientWithID($id);
			$client->update(array(
				self::COLUMN_PERSONAL_NUMBER => $personal_number,
				self::COLUMN_PERSONAL_ID => $personal_id,
				self::COLUMN_NAME => $name,
				self::COLUMN_SURNAME => $surname,
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_PHONE => $phone,
				self::COLUMN_EMAIL => $email,
				self::COLUMN_BRANCH => $branch,
				self::COLUMN_BIRTHDAY_NUMBER => $birthday_number,
				self::COLUMN_SEX => $sex
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function clientHasImage($id) {
		try {
			$client = $this->getClientWithID($id);
			$client->update(array(
				self::COLUMN_HAS_IMAGE => 1,
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function hasImage($id) {
		try {
			$client = $this->getClientWithID($id);
			return $client->has_image;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function deleteClient($id) {
		try {
			$client = $this->getClientWithID($id);
			$client->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function searchClientByName($name) {
		$client = $this->database->query('SELECT * FROM ' . self::TABLE_NAME . ' WHERE  CONCAT(`' . self::COLUMN_NAME . '`, \' \', `'. self::COLUMN_SURNAME . '`) LIKE \'%' . $name . '%\'');

		return $client;
	}

	public function searchClientByPersonalID($personalID) {
		$client = $this->database->query('SELECT * FROM ' . self::TABLE_NAME . ' WHERE ' . self::COLUMN_PERSONAL_ID . ' LIKE \'%' . $personalID . '%\' LIMIT 20');

		return $client;
	}

	public function getAccountsForClient($clientID) {
		$accounts = $this->database->table(self::ACCOUNT_TABLE_NAME)->where('owner = ?', $clientID);

		return $accounts;
	}

	public function getSharedAccountsForClient($clientID) {
		$this->accessManager = new AccessManager($this->database);

		$access = $this->accessManager->getAccessAllByClientID($clientID);

		$accounts = array();

		foreach ($access as $one) {
			$account = $this->getAccountWithID($one->account_id);

			array_push($accounts, $account);
		}

		return $accounts;
	}

	public function addNewAccount($owner, $name, $type, $amount, $interest, $daily_limit) {
		try {
			switch($type) {
				case 1: {
					$row = $this->database->table(self::REGULAR_ACCOUNT_TABLE_NAME)->insert(array(
						self::ACCOUNT_DAILY_LIMIT => $daily_limit,
					));

					$typeName = "regular";

					break;
				}
				case 2: {
					$row = $this->database->table(self::SAVINGS_ACCOUNT_TABLE_NAME)->insert(array(
						self::ACCOUNT_INTEREST => $interest,
					));

					$typeName = "savings";

					break;
				}
				case 3: {
					$row = $this->database->table(self::CREDIT_ACCOUNT_TABLE_NAME)->insert(array(
						self::ACCOUNT_AMOUNT => $amount,
						self::ACCOUNT_INTEREST => $interest,
					));

					$typeName = "credit";

					break;
				}
			}

			$row = $this->database->table(self::ACCOUNT_TABLE_NAME)->insert(array(
				self::ACCOUNT_BALANCE => 0,
				self::ACCOUNT_DATE_CREATED => date('Y-m-d G:i:s'),
				self::ACCOUNT_OWNER => $owner,
				self::ACCOUNT_NAME => $name,
				self::ACCOUNT_TYPE => $typeName,
				self::ACCOUNT_TYPE_ID => $row
			));

			return $row->id;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function getAccountWithID($accountID) {
		$account = $this->database->table(self::ACCOUNT_TABLE_NAME)->get($accountID);

		return $account;
	}

	public function getSpecificAccountType($type, $typeID) {
		if ($type == "regular") {
			$specific = $this->database->table(self::REGULAR_ACCOUNT_TABLE_NAME)->get($typeID);
		} elseif ($type == "savings") {
			$specific = $this->database->table(self::SAVINGS_ACCOUNT_TABLE_NAME)->get($typeID);
		} else {
			$specific = $this->database->table(self::CREDIT_ACCOUNT_TABLE_NAME)->get($typeID);
		}

		return $specific;
	}

	public function sendAmount($accountID, $amount) {
		$account = $this->getAccountWithID($accountID);

		$account->update(array(
			self::ACCOUNT_BALANCE => $account->balance - $amount
			));
	}

	public function raiseAmount($accountID, $amount) {
		$account = $this->getAccountWithID($accountID);

		$account->update(array(
			self::ACCOUNT_BALANCE => $account->balance + $amount
			));
	}

/*
	public function updateClient($id, $name, $surname, $personal_number, $personal_id, $adress, $city, $zip, $phone, $email) {
		try {
			$client = $this->getClientWithID($id);
			$client->update(array(
				self::COLUMN_PERSONAL_NUMBER => $personal_number,
				self::COLUMN_PERSONAL_ID => $personal_id,
				self::COLUMN_NAME => $name,
				self::COLUMN_SURNAME => $surname,
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_PHONE => $phone,
				self::COLUMN_EMAIL => $email,
				self::COLUMN_BRANCH => $branch
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}
*/

	public function updateAccount($account_id, $daily_limit) {
		try {
			$account = $this->getAccountWithID($account_id);

			$regular = $this->getSpecificAccountType($account->type, $account->type_id);

			$regular->update(array(
				self::ACCOUNT_DAILY_LIMIT => $daily_limit
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function deleteAccount($id) {
		try {
			$account = $this->getAccountWithID($id);
			$account->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}
}