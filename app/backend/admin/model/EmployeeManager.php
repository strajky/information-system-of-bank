<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;


/**
 * Users management.
 */
class EmployeeManager extends Nette\Object {
	const
		TABLE_NAME = 'employee',
		COLUMN_ID = 'id',
		COLUMN_USERNAME = 'username',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_ROLE = 'role',
		COLUMN_PERSONAL_NUMBER = 'personal_number',
		COLUMN_PERSONAL_ID = 'personal_id',
		COLUMN_NAME = 'name',
		COLUMN_SURNAME = 'surname',
		COLUMN_ADRESS = 'adress',
		COLUMN_CITY = 'city',
		COLUMN_ZIP = 'zip',
		COLUMN_PHONE = 'phone',
		COLUMN_EMAIL = 'email',
		COLUMN_BRANCH = 'branch',
		COLUMN_BIRTHDAY_NUMBER = 'birthday_number',
		COLUMN_SEX = 'sex',
		COLUMN_HAS_IMAGE = 'has_image';



	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function generatePassword($length) {
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);

	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }

	    return $result;
	}

	public function generateUsername($name, $surname) {
		$name = strtolower(Settings::convertToASCII($name));
		$surname = strtolower(Settings::convertToASCII($surname));

		$surnamePart = substr($surname, 0, 5);
		$namePart = "";

		if (strlen($surnamePart) < 5) {
			$charactersNeeded = 5 - strlen($surnamePart);

			$namePart = substr($name, 0, $charactersNeeded);
		}

		$newUsername = "x" . $surnamePart . $namePart;

		$users = $this->database->query('SELECT * FROM ' . self::TABLE_NAME . ' WHERE `' . self::COLUMN_USERNAME . '` LIKE \'%' . $newUsername . '%\'');

		$allUsers = $users->fetchAll();

		if (count($allUsers) > 0) {
			$last = end($allUsers);
			$lastUsername = $last->username;
			$lastNumber = substr($lastUsername, -2, 2);
			$lastNumber = intval($lastNumber);
			$lastNumber++;

			if ($lastNumber < 10) {
				$newUsername .= "0";
				$newUsername .= $lastNumber;
			} else {
				$newUsername .= $lastNumber;
			}
		} else {
			$newUsername .= "01";
		}

		return $newUsername;
	}

	public function addNewEmployee($name, $surname, $personal_number, $personal_id, $adress, $city, $zip, $phone, $email, $branch, $birthday_number, $sex) {
		$password = $this::generatePassword(8);
		$username = $this::generateUsername($name, $surname);

		try {
			$mail = new Message;
			$mail->setFrom('adam.konecny94@gmail.com')
				->addTo($email)
				->setSubject('Přihlašovací údaje')
				->setBody('Dobrý den, byly Vám vygenerovány přihlašovací údaje do systému. Uživatelské jméno je \'' . $username . '\' a heslo je \'' . $password . '\'');

			$mailer = new SendmailMailer;

			$row = $this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_USERNAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_ROLE => 'employee',
				self::COLUMN_PERSONAL_NUMBER => $personal_number,
				self::COLUMN_PERSONAL_ID => $personal_id,
				self::COLUMN_NAME => $name,
				self::COLUMN_SURNAME => $surname,
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_PHONE => $phone,
				self::COLUMN_EMAIL => $email,
				self::COLUMN_BRANCH => $branch,
				self::COLUMN_BIRTHDAY_NUMBER => $birthday_number,
				self::COLUMN_SEX => $sex
			));

			$mailer->send($mail);

			return $row->id;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}

		return NULL;
	}

	public function updateEmployee($id, $name, $surname, $personal_number, $personal_id, $adress, $city, $zip, $phone, $email, $branch, $birthday_number, $sex) {
		try {
			$employee = $this->getEmployeeWithID($id);
			$employee->update(array(
				self::COLUMN_PERSONAL_NUMBER => $personal_number,
				self::COLUMN_PERSONAL_ID => $personal_id,
				self::COLUMN_NAME => $name,
				self::COLUMN_SURNAME => $surname,
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_PHONE => $phone,
				self::COLUMN_EMAIL => $email,
				self::COLUMN_BRANCH => $branch,
				self::COLUMN_BIRTHDAY_NUMBER => $birthday_number,
				self::COLUMN_SEX => $sex
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function employeeHasImage($id) {
		try {
			$employee = $this->getEmployeeWithID($id);
			$employee->update(array(
				self::COLUMN_HAS_IMAGE => 1,
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function hasImage($id) {
		try {
			$employee = $this->getEmployeeWithID($id);
			return $employee->has_image;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function deleteEmployee($id) {
		try {
			$employee = $this->getEmployeeWithID($id);
			$employee->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function changePassword($id, $oldPassword, $newPassword) {
		$employee = $this->getEmployeeWithID($id);

		try {
			if (!Passwords::verify($oldPassword, $employee[self::COLUMN_PASSWORD_HASH])) {
				throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

				return 1;
			} else {
				$employee->update(array(
					self::COLUMN_PASSWORD_HASH => Passwords::hash($newPassword),
				));
			}
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');

				return 2;
		}

		return 0;
	}

	public function searchEmployeeByName($name) {
		$employees = $this->database->query('SELECT * FROM ' . self::TABLE_NAME . ' WHERE  CONCAT(`' . self::COLUMN_NAME . '`, \' \', `'. self::COLUMN_SURNAME . '`) LIKE \'%' . $name . '%\'');

		return $employees;
	}

	public function getEmployeeWithID($employeeID) {
		return $this->database->table(self::TABLE_NAME)->get($employeeID);
	}
}