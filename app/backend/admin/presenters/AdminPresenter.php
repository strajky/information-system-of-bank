<?php

namespace App\Presenters;

use Nette;
use App\Model\Settings;
use App\Model\EmployeeManager;
use App\Model\BranchManager;
use App\Model\ClientManager;
use App\Model\TransactionsManager;
use App\Model\AccessManager;
use Nette\Application\UI\Form;


class AdminPresenter extends BackendPresenter
{
	public $employeeManager;
	public $branchManager;
	public $clientManager;
	public $transactionsManager;
	public $accessManager;

	public function beforeRender() {
		parent::beforeRender();

		if ($this->user->getIdentity()->role == "client") {
			$this->getPresenter()->redirect('Homepage:');
		}

		$this->loadManagers();

		$this->template->detail = "none";

		$this->template->role = $this->user->getIdentity()->role;
		$this->template->userName = $this->user->getIdentity()->name;
		$this->template->employeeID = $this->user->getIdentity()->id;
		$this->template->hasImage = $this->employeeManager->hasImage($this->user->getIdentity()->id);
	}

	public function beforeValidation() {
		parent::beforeValidation();

		$this->loadManagers();
	}

	protected function loadManagers() {
		$this->branchManager = new BranchManager($this->database);
		$this->employeeManager = new EmployeeManager($this->database);
		$this->clientManager = new ClientManager($this->database);
		$this->accessManager = new AccessManager($this->database);
		$this->transactionsManager = new TransactionsManager($this->database);
	}

//  *  *  *  *  *  *  *  *  *  *  *  USER DETAIL  *  *  *  *  *  *  *  *  *  *  *
	public function renderDefault() {
		$this->template->submenu = "user";

		$this->template->name = $this->user->getIdentity()->name;
		$this->template->surname = $this->user->getIdentity()->surname;
		
		$this->template->employee = $this->employeeManager->getEmployeeWithID($this->user->getIdentity()->id);
		$this->template->branch = $this->branchManager->getBranchWithID($this->user->getIdentity()->branch);
	}

//  *  *  *  *  *  *  *  *  *  *  *  ADD EMPLOYEE  *  *  *  *  *  *  *  *  *  *  *
	public function renderAddEmployee() {
		$this->loadManagers();
	}

	protected function createComponentAddEmployeeForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno*")
			->setRequired('Musíte zadat jméno')
			->addRule(Form::MIN_LENGTH, 'Jméno musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Jméno může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Jméno*');

		$form->addText('surname', 'Příjmení')
			->setAttribute('placeholder', "Příjmení*")
			->setRequired('Musíte zadat příjmeni')
			->addRule(Form::MIN_LENGTH, 'Přijmení musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Příjmení může mít maximálně %d znaků', 60)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Příjmení*');

		$form->addText('personal_number_first', 'Rodné číslo')
			->setAttribute('placeholder', "Rodné číslo*")
			->setRequired('Musíte zadat rodné číslo')
			->addRule(Form::PATTERN, 'První část rodného čísla musí mít 6 číslic', '([0-9]\s*){6}')
			->setAttribute('class', 'person_num_first')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 1.část*');

		$form->addText('personal_number_second', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->addRule(Form::PATTERN, 'Druhá část rodného čísla musí mít 4 číslice', '([0-9]\s*){4}')
			->setAttribute('class', 'person_num_second')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 2.část*');

		$form->addText('personal_id', 'Číslo občanského průkazu')
			->setAttribute('placeholder', "Číslo občanského průkazu*")
			->setRequired('Musíte zadat číslo občanského průkazu')
			->addRule(Form::PATTERN, 'Číslo občanského průkazu musí mít 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'personal_id')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo občanského průkazu*');

		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('class', 'adress')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('class', 'city')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('class', 'zip')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addText('predvolba', 'Předvolba')
			->setAttribute('placeholder', "+420")
			->setDefaultValue('+420')
			->setRequired('Musíte zadat předvolbu')
			->addRule(Form::PATTERN, 'Špatně zadaná předvolba', '^\+[0-9]{3}$')
			->setAttribute('class', 'preset')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Předvolba');

		$form->addText('phone', 'Telefon')
			->setAttribute('placeholder', "Telefon*")
			->setRequired('Musíte zadat Telefon')
			->addRule(Form::PATTERN, 'Telefon musí mít přesně 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'phone')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Telefon*');

		$form->addText('email', 'E-mail')
			->setAttribute('placeholder', "E-mail*")
			->setRequired('Musíte zadat e-mail')
			->addRule(Form::PATTERN, 'E-mailová adresa není validní', '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
			->setAttribute('class', 'email')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'E-mail*');

		$allBranches = $this->branchManager->getAllBranches();

		$branches = array();

		foreach ($allBranches as $branch) {
			$branches[$branch->id] = $branch->adress . ", " . $branch->city;
		}

		$form->addSelect('branch', 'Pobočka', $branches)
			->setPrompt('Vyber pobočku');

		$form->addSubmit('send', 'Přidat zaměstnance');

		$form->onSuccess[] = $this->addEmployeeFormSucceeded;

		return $form;
	}

	public function addEmployeeFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$personal_number = $values->personal_number_first . "/" . $values->personal_number_second;
			$arr = ($this->parsePersonaNumber(strval($values->personal_number_first.$values->personal_number_second)));

			$phone = $values->predvolba . $values->phone;
			$zip = preg_replace('/\s+/', '', $values->zip);

			$employeeID = $this->employeeManager->addNewEmployee($values->name, $values->surname, $personal_number, $values->personal_id, $values->adress, $values->city, $zip, $phone, $values->email, $values->branch, $arr[0], $arr[1]);

			$this->flashMessage('Nový uživatel byl úspěšně vytvořen.');

			if ($employeeID) {
				$this->redirect("Admin:employeeDetail", $employeeID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se vytvořit nového uživatele.');
		}
	}

	public function parsePersonaNumber($personal_number) {		
		$arr[0] = NULL;
		$arr[1] = 'muž';

		$year = floor($personal_number / 100000000);
		$month = ($personal_number / 1000000) % 100;
		$day = ($personal_number / 10000) % 100;

		// Rodné číslo musí být dělitelné 11
		if (($personal_number % 11) != 0) {
			throw new \Exception('Neplatné rodné číslo.');
			return;
		}

		// Pokud je měsíc větš > 50 -> je to žena, jinak muž
		if ($month > 50) {
			$month = $month - 50;
			$arr[1] = 'žena';
		}

		$arr[0] = "$day"."."."$month"."."."19"."$year";		// birthday number
		
		return $arr;
	}

//  *  *  *  *  *  *  *  *  *  *  *  ADD BRANCH  *  *  *  *  *  *  *  *  *  *  *
	public function renderAddBranch() {
		$this->loadManagers();
	}

	protected function createComponentAddBranchForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addTextArea('description', 'Popis')
			->setAttribute('placeholder', "Popis*")
			->setRequired('Musíte zadat popis')
			->addRule(Form::MAX_LENGTH, 'Poznámka může mít maximálně %d znaků', 2000)
			->setAttribute('class', 'description')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Popis*');

		$form->addRadioList('atm', 'Bankomat', array(
			'1' => 'Ano',
			'0' => 'Ne'
			))
			->setDefaultValue('0');

		$form->addSubmit('send', 'Přidat pobočku');

		$form->onSuccess[] = $this->addBranchFormSucceeded;

		return $form;
	}

	public function addBranchFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$zip = preg_replace('/\s+/', '', $values->zip);

			$branchID = $this->branchManager->addNewBranch($values->adress, $values->city, $zip, $values->atm, $values->description);

			if ($branchID) {
				$this->redirect("Admin:branchDetail", $branchID);
			}

			$this->flashMessage('Nová pobočka byla úspěšně vytvořena.');
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se vytvořit novou pobočku.');
		}
	}

//  *  *  *  *  *  *  *  *  *  *  *  SEARCH EMPLOYEE  *  *  *  *  *  *  *  *  *  *  *
	public function renderSearchEmployee() {
		if (!isset($this->template->results)) {
			$this->template->results = NULL;
		}
	}

	protected function createComponentSearchEmployeeForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno Příjmení")
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Zadejte jméno a příjmeni pro vyhledání zaměstnance');

		$form->addSubmit('send', 'Hledat')
			->setAttribute('id', 'search-button')
			->setAttribute('class', "ajax");

		$form->onSuccess[] = $this->searchEmployeeFormSucceeded;

		return $form;
	}

	public function searchEmployeeFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$this->template->results = $this->employeeManager->searchEmployeeByName($values->name);

		if ($this->isAjax()) {
			$this->redrawControl('searchResults');
		}
	}

//  *  *  *  *  *  *  *  *  *  *  *  MANAGE TIME  *  *  *  *  *  *  *  *  *  *  *
	public function renderManageTime() {
		
		$this->template->time = date('H:i:s');
		$this->template->date = date('d.m.Y');
	}

//  *  *  *  *  *  *  *  *  *  *  *  EMPLOYEE DETAIL  *  *  *  *  *  *  *  *  *  *  *
	public function renderEmployeeDetail($employeeID) {
		$this->template->submenu = "employee";
		$this->template->employeeID = $employeeID;

		$this->template->employee = $this->employeeManager->getEmployeeWithID($employeeID);
		$this->template->branch = $this->branchManager->getBranchWithID($this->template->employee->branch);
	}

//  *  *  *  *  *  *  *  *  *  *  *  EDIT EMPLOYEE  *  *  *  *  *  *  *  *  *  *  *
	public function renderEditEmployee($employeeID) {
		$this->template->submenu = "employee";
		$this->template->employeeID = $employeeID;
	}

	protected function createComponentEditEmployeeForm() {
		$this->loadManagers();

		$form = new Form;

		$employeeID = $this->getParameter('employeeID');

		$employee = $this->employeeManager->getEmployeeWithID($employeeID);

		$personal_number = $employee->personal_number;
		$personal_number = explode('/', $personal_number);

		$phone = $employee->phone;
		$phone = substr($phone, -9, 9);

		$predvolba = substr($employee->phone, 0, strlen($employee->phone) - 9);

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno*")
			->setRequired('Musíte zadat jméno')
			->addRule(Form::MIN_LENGTH, 'Jméno musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Jméno může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Jméno*');

		$form->addText('surname', 'Příjmení')
			->setAttribute('placeholder', "Příjmení*")
			->setRequired('Musíte zadat příjmeni')
			->addRule(Form::MIN_LENGTH, 'Přijmení musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Příjmení může mít maximálně %d znaků', 60)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Přijmení');

		$form->addText('personal_number_first', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->setDefaultValue($personal_number[0])
			->addRule(Form::PATTERN, 'První část rodného čísla musí mít 6 číslic', '([0-9]\s*){6}')
			->setAttribute('class', 'person_num_first')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 1.část*');

		$form->addText('personal_number_second', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->setDefaultValue($personal_number[1])
			->addRule(Form::PATTERN, 'Druhá část rodného čísla musí mít 4 číslice', '([0-9]\s*){4}')
			->setAttribute('class', 'person_num_second')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 2.část*');


		$form->addText('personal_id', 'Číslo občanského průkazu')
			->setRequired('Musíte zadat číslo občanského průkazu')
			->addRule(Form::PATTERN, 'Číslo občanského průkazu musí mít 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'personal_id')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo občanského průkazu*');


		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('class', 'adress')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('class', 'city')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('class', 'zip')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addText('predvolba', 'Předvolba')
			->setAttribute('placeholder', "+420")
			->setDefaultValue($predvolba)
			->setRequired('Musíte zadat předvolbu')
			->addRule(Form::PATTERN, 'Špatně zadaná předvolba', '^\+[0-9]{3}$')
			->setAttribute('class', 'preset')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Předvolba');


		$form->addText('phoneShort', 'Telefon')
			->setAttribute('placeholder', "Telefon*")
			->setDefaultValue($phone)
			->setRequired('Musíte zadat Telefon')
			->addRule(Form::PATTERN, 'Telefon musí mít přesně 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'phone')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Telefon*');


		$form->addText('email', 'E-mail')
			->setAttribute('placeholder', "E-mail*")
			->setRequired('Musíte zadat e-mail')
			->addRule(Form::PATTERN, 'E-mailová adresa není validní', '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
			->setAttribute('class', 'email')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'E-mail*');

		$form->addHidden('id')
			->setDefaultValue($employeeID);

		$allBranches = $this->branchManager->getAllBranches();


		$branches = array();

		foreach ($allBranches as $branch) {
			$branches[$branch->id] = $branch->adress . ", " . $branch->city;
		}

		$form->addSelect('branch', 'Pobočka', $branches)
			->setPrompt('Vyber pobočku');

		$form->addSubmit('send', 'Upravit zaměstnance');

		$form->setDefaults($employee);

		$form->onSuccess[] = $this->editEmployeeFormSucceeded;

		return $form;
	}

	public function editEmployeeFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$personal_number = $values->personal_number_first . "/" . $values->personal_number_second;
			$arr = ($this->parsePersonaNumber(strval($values->personal_number_first.$values->personal_number_second)));
			$phone = $values->predvolba . $values->phoneShort;
			$zip = preg_replace('/\s+/', '', $values->zip);

			$this->employeeManager->updateEmployee($values->id, $values->name, $values->surname, $personal_number, $values->personal_id, $values->adress, $values->city, $zip, $phone, $values->email, $values->branch, $arr[0], $arr[1]);

			$this->flashMessage('Uživatel byl úspěšně upraven.');

			if ($values->id) {
				$this->redirect("Admin:employeeDetail", $values->id);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se upravit uživatele.');
		}
	}

//  *  *  *  *  *  *  *  *  *  *  *  DELETE EMPLOYEE  *  *  *  *  *  *  *  *  *  *  *
	public function renderDeleteEmployee($employeeID) {
		$this->template->submenu = "employee";
		$this->template->employeeID = $employeeID;

		$employee = $this->employeeManager->getEmployeeWithID($employeeID);

		$this->template->employee = $employee;
	}

	public function handleDeleteEmployee($employeeID) {
		$this->beforeValidation();

		$this->employeeManager->deleteEmployee($employeeID);

		$this->flashMessage('Zaměstnanec byl úspěšně odstraněn.');

		$this->redirect("Admin:searchEmployee");
	}

//  *  *  *  *  *  *  *  *  *  *  *  MANAGE TIME  *  *  *  *  *  *  *  *  *  *  *
	public function renderBranches() {
		$this->template->results = $this->branchManager->getAllBranches();
	}

//  *  *  *  *  *  *  *  *  *  *  *  BRANCH DETAIL  *  *  *  *  *  *  *  *  *  *  *
	public function renderBranchDetail($branchID) {
		$this->template->submenu = "branch";
		$this->template->branchID = $branchID;

		$this->template->branch = $this->branchManager->getBranchWithID($branchID);
		$this->template->employee = $this->employeeManager->getEmployeeWithID($this->template->branch->manager);

	}

//  *  *  *  *  *  *  *  *  *  *  *  EDIT BRANCH  *  *  *  *  *  *  *  *  *  *  *
	public function renderEditBranch($branchID) {
		$this->template->submenu = "branch";
		$this->template->branchID = $branchID;
	}

	protected function createComponentEditBranchForm() {
		$this->loadManagers();

		$form = new Form;

		$branchID = $this->getParameter('branchID');

		$branch = $this->branchManager->getBranchWithID($branchID);

		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addTextArea('description', 'Popis')
			->setAttribute('placeholder', "Popis*")
			->setRequired('Musíte zadat popis')
			->addRule(Form::MAX_LENGTH, 'Poznámka může mít maximálně %d znaků', 2000)
			->setAttribute('class', 'description')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Popis*');

		$form->addRadioList('atm', 'Bankomat', array(
			'1' => 'Ano',
			'0' => 'Ne'
			));

		$form->addHidden('id')
			->setDefaultValue($branchID);

		$form->addSubmit('send', 'Upravit pobočku');

		$form->setDefaults($branch);

		$form->onSuccess[] = $this->editBranchFormSucceeded;

		return $form;
	}

	public function editBranchFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$zip = preg_replace('/\s+/', '', $values->zip);

			$this->branchManager->updateBranch($values->id, $values->adress, $values->city, $zip, $values->atm, $values->description);

			$this->flashMessage('Pobočka byla úspěšně upravena.');

			if ($values->id) {
				$this->redirect("Admin:branchDetail", $values->id);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se upravit pobočku.');
		}
	}

//  *  *  *  *  *  *  *  *  *  *  *  DELETE BRANCH  *  *  *  *  *  *  *  *  *  *  *
	public function renderDeleteBranch($branchID) {
		$this->template->submenu = "branch";
		$this->template->branchID = $branchID;

		$branch = $this->branchManager->getBranchWithID($branchID);

		$this->template->branch = $branch;
	}

	public function handleDeleteBranch($branchID) {
		$this->beforeValidation();

		$this->branchManager->deleteBranch($branchID);

		$this->flashMessage('Pobočka byla úspěšně odstraněna.');

		$this->redirect("Admin:branches");
	}

//  *  *  *  *  *  *  *  *  *  *  *  CHANGE PASSWORD  *  *  *  *  *  *  *  *  *  *  *
	public function createComponentChangePasswordForm() {
    	$this->loadManagers();

    	$form = new Form;

		$form->addPassword('oldPassword', '')
		    ->addRule(Form::FILLED, 'Zadejte Vaše staré heslo')
		    ->setAttribute('placeholder', 'Původní heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Staré heslo');
		$form->addPassword('newPassword', '')
		    ->addRule(Form::FILLED, 'Zadejte nové heslo')
		    ->setAttribute('placeholder', 'Nové heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Nové heslo');
		$form->addPassword('newPassword2', '')
		    ->addRule(Form::FILLED, 'Znovu zadejte nové heslo')
		    ->addRule(Form::EQUAL, "Nové heslo se neshoduje", $form["newPassword"])
		    ->setAttribute('placeholder', 'Znovu nové heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Nové heslo');

    	$form->addSubmit('send', 'Změnit');

    	$form->onSuccess[] = $this->processChangePasswordForm;

    	return $form;
    }

    public function processChangePasswordForm($form) {
    	$this->loadManagers();

        $values = $form->getValues();

        $userId = $this->getUser()->id;
        $oldPassword = $values["oldPassword"];
        $newPassword = $values["newPassword"];

        try {
        	$ret = $this->employeeManager->changePassword($userId, $oldPassword, $newPassword);

            if ($ret == 0) {
            	$this->flashMessage('Heslo bylo úspěšně změněno.');
            	$this->redirect('Admin:');
            } else if ($ret == 1) {
            	$this->flashMessage('Původní heslo je chybné.');
            }
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Změna hesla se nezdařila.');
        }

    }

//  *  *  *  *  *  *  *  *  *  *  *  ADD CLIENT  *  *  *  *  *  *  *  *  *  *  *
	public function renderAddClient() {
		$this->loadManagers();
	}

	protected function createComponentAddClientForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno*")
			->setRequired('Musíte zadat jméno')
			->addRule(Form::MIN_LENGTH, 'Jméno musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Jméno může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Jméno*');

		$form->addText('surname', 'Příjmení')
			->setAttribute('placeholder', "Příjmení*")
			->setRequired('Musíte zadat příjmeni')
			->addRule(Form::MIN_LENGTH, 'Přijmení musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Příjmení může mít maximálně %d znaků', 60)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Příjmení*');

		$form->addText('personal_number_first', 'Rodné číslo')
			->setAttribute('placeholder', "Rodné číslo*")
			->setRequired('Musíte zadat rodné číslo')
			->addRule(Form::PATTERN, 'První část rodného čísla musí mít 6 číslic', '([0-9]\s*){6}')
			->setAttribute('class', 'person_num_first')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 1.část*');

		$form->addText('personal_number_second', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->addRule(Form::PATTERN, 'Druhá část rodného čísla musí mít 4 číslice', '([0-9]\s*){4}')
			->setAttribute('class', 'person_num_second')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 2.část*');

		$form->addText('personal_id', 'Číslo občanského průkazu')
			->setAttribute('placeholder', "Číslo občanského průkazu*")
			->setRequired('Musíte zadat číslo občanského průkazu')
			->addRule(Form::PATTERN, 'Číslo občanského průkazu musí mít 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'personal_id')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo občanského průkazu*');

		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('class', 'adress')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('class', 'city')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('class', 'zip')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addText('predvolba', 'Předvolba')
			->setAttribute('placeholder', "+420")
			->setDefaultValue('+420')
			->setRequired('Musíte zadat předvolbu')
			->addRule(Form::PATTERN, 'Špatně zadaná předvolba', '^\+[0-9]{3}$')
			->setAttribute('class', 'preset')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Předvolba');

		$form->addText('phone', 'Telefon')
			->setAttribute('placeholder', "Telefon*")
			->setRequired('Musíte zadat Telefon')
			->addRule(Form::PATTERN, 'Telefon musí mít přesně 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'phone')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Telefon*');

		$form->addText('email', 'E-mail')
			->setAttribute('placeholder', "E-mail*")
			->setRequired('Musíte zadat e-mail')
			->addRule(Form::PATTERN, 'E-mailová adresa není validní', '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
			->setAttribute('class', 'email')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'E-mail*');

		$form->addSubmit('send', 'Přidat klienta');

		$form->onSuccess[] = $this->addClientFormSucceeded;

		return $form;
	}

	public function addClientFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$personal_number = $values->personal_number_first . "/" . $values->personal_number_second;
			$arr = ($this->parsePersonaNumber(strval($values->personal_number_first.$values->personal_number_second)));
			$phone = $values->predvolba . $values->phone;
			$zip = preg_replace('/\s+/', '', $values->zip);

			$clientID = $this->clientManager->addNewClient($values->name, $values->surname, $personal_number, $values->personal_id, $values->adress, $values->city, $zip, $phone, $values->email, $arr[0], $arr[1]);

			$this->flashMessage('Nový klient byl úspěšně vytvořen.');

			if ($clientID) {
				$this->redirect("Admin:clientDetail", $clientID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se vytvořit nového klienta.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  CLIENT DETAIL  *  *  *  *  *  *  *  *  *  *  *
	public function renderClientDetail($clientID) {
		$this->template->submenu = "client";
		$this->template->detail = "clientDetail";

		$this->template->clientID = $clientID;

		$this->template->client = $this->clientManager->getClientWithID($clientID);

		$this->template->accounts = $this->clientManager->getAccountsForClient($clientID);
	}

	//  *  *  *  *  *  *  *  *  *  *  *  SEARCH CLIENT  *  *  *  *  *  *  *  *  *  *  *
	public function renderSearchClient() {
		if (!isset($this->template->results)) {
			$this->template->results = NULL;
		}
	}

	protected function createComponentSearchClientForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno Příjmení")
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Jméno Příjmení');

		$form->addSubmit('send', 'Hledat')
			->setAttribute('id', 'search-button')
			->setAttribute('class', "ajax");

		$form->onSuccess[] = $this->searchClientFormSucceeded;

		return $form;
	}

	public function searchClientFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$this->template->results = $this->clientManager->searchClientByName($values->name);

		if ($this->isAjax()) {
			$this->redrawControl('searchResults');
		}
	}

//  *  *  *  *  *  *  *  *  *  *  *  EDIT CLIENT  *  *  *  *  *  *  *  *  *  *  *
	public function renderEditClient($clientID) {
		$this->template->submenu = "client";
		$this->template->detail = "none";
		$this->template->clientID = $clientID;
	}

	protected function createComponentEditClientForm() {
		$this->loadManagers();

		$form = new Form;

		$clientID = $this->getParameter('clientID');

		$client = $this->clientManager->getClientWithID($clientID);

		$personal_number = $client->personal_number;
		$personal_number = explode('/', $personal_number);

		$phone = $client->phone;
		$phone = substr($phone, -9, 9);

		$predvolba = substr($client->phone, 0, strlen($client->phone) - 9);

		$form->addText('name', 'Jméno')
			->setAttribute('placeholder', "Jméno*")
			->setRequired('Musíte zadat jméno')
			->addRule(Form::MIN_LENGTH, 'Jméno musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Jméno může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Jméno*');

		$form->addText('surname', 'Příjmení')
			->setAttribute('placeholder', "Příjmení*")
			->setRequired('Musíte zadat příjmeni')
			->addRule(Form::MIN_LENGTH, 'Přijmení musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Příjmení může mít maximálně %d znaků', 60)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž]+$')
			->setAttribute('class', 'name')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Příjmení*');

		$form->addText('personal_number_first', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->setDefaultValue($personal_number[0])
			->addRule(Form::PATTERN, 'První část rodného čísla musí mít 6 číslic', '([0-9]\s*){6}')
			->setAttribute('class', 'person_num_first')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 1.část*');

		$form->addText('personal_number_second', 'Rodné číslo')
			->setRequired('Musíte zadat rodné číslo')
			->setDefaultValue($personal_number[1])
			->addRule(Form::PATTERN, 'Druhá část rodného čísla musí mít 4 číslice', '([0-9]\s*){4}')
			->setAttribute('class', 'person_num_second')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Rodné číslo 2.část*');

		$form->addText('personal_id', 'Číslo občanského průkazu')
			->setRequired('Musíte zadat číslo občanského průkazu')
			->addRule(Form::PATTERN, 'Číslo občanského průkazu musí mít 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'personal_id')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo občanského průkazu*');

		$form->addText('adress', 'Adresa')
			->setAttribute('placeholder', "Adresa*")
			->setRequired('Musíte zadat adresu')
			->addRule(Form::MIN_LENGTH, 'Adresa musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Adresa může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Adresa obsahuje nedovolené znaky', '^[0-9A-Ža-ž. -/]+$')
			->setAttribute('class', 'adress')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Adresa*');

		$form->addText('city', 'Město')
			->setAttribute('placeholder', "Město*")
			->setRequired('Musíte zadat město')
			->addRule(Form::MIN_LENGTH, 'Město musí mít alespoň %d znaky', 3)
			->addRule(Form::MAX_LENGTH, 'Město může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Město obsahuje nedovolené znaky', '^[A-Ža-ž ]+$')
			->setAttribute('class', 'city')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Město*');

		$form->addText('zip', 'PSČ')
			->setAttribute('placeholder', "PSČ*")
			->setRequired('Musíte zadat PSČ')
			->addRule(Form::PATTERN, 'PSČ musí mít přesně 5 číslic', '([0-9]\s*){5}')
			->setAttribute('class', 'zip')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'PSČ*');

		$form->addText('predvolba', 'Předvolba')
			->setAttribute('placeholder', "+420")
			->setDefaultValue($predvolba)
			->setRequired('Musíte zadat předvolbu')
			->addRule(Form::PATTERN, 'Špatně zadaná předvolba', '^\+[0-9]{3}$')
			->setAttribute('class', 'preset')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Předvolba');

		$form->addText('phoneShort', 'Telefon')
			->setAttribute('placeholder', "Telefon*")
			->setDefaultValue($phone)
			->setRequired('Musíte zadat Telefon')
			->addRule(Form::PATTERN, 'Telefon musí mít přesně 9 číslic', '([0-9]\s*){9}')
			->setAttribute('class', 'phone')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Telefon*');

		$form->addText('email', 'E-mail')
			->setAttribute('placeholder', "E-mail*")
			->setRequired('Musíte zadat e-mail')
			->addRule(Form::PATTERN, 'E-mailová adresa není validní', '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
			->setAttribute('class', 'email')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'E-mail*');

		$form->addHidden('id')
			->setDefaultValue($clientID);

		$form->addSubmit('send', 'Upravit klienta');

		$form->setDefaults($client);

		$form->onSuccess[] = $this->editClientFormSucceeded;

		return $form;
	}

	public function editClientFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();
		
		try {
			$personal_number = $values->personal_number_first . "/" . $values->personal_number_second;
			$phone = $values->predvolba . $values->phoneShort;
			$zip = preg_replace('/\s+/', '', $values->zip);

			$this->clientManager->updateClient($values->id, $values->name, $values->surname, $personal_number, $values->personal_id, $values->adress, $values->city, $zip, $phone, $values->email, $arr[0], $arr[1]);

			$this->flashMessage('Klient byl úspěšně upraven.');

			if ($values->id) {
				$this->redirect("Admin:clientDetail", $values->id);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se upravit klienta.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  DELETE CLIENT  *  *  *  *  *  *  *  *  *  *  *
	public function renderDeleteClient($clientID) {
		$this->template->submenu = "client";
		$this->template->detail = "none";
		$this->template->clientID = $clientID;

		$client = $this->clientManager->getClientWithID($clientID);

		$this->template->client = $client;
	}

	public function handleDeleteClient($clientID) {
		$this->beforeValidation();

		$this->clientManager->deleteClient($clientID);

		$this->flashMessage('Klient byl úspěšně odstraněn.');

		$this->redirect("Admin:searchClient");
	}

	//  *  *  *  *  *  *  *  *  *  *  *  ADD EMPLOYEE  *  *  *  *  *  *  *  *  *  *  *
	public function renderAddAccount($clientID) {
		$this->template->submenu = "client";
		$this->template->detail = "none";
		$this->template->clientID = $clientID;
	}

	public function validateForm($form) {
		$this->beforeValidation();

		$clientID = $this->getParameter('clientID');

		$values = $form->getValues();
		
		try {
			switch ($values->account_type) {
				case '1': {
					$accountID = $this->clientManager->addNewAccount($clientID, $values->name, $values->account_type, null, null, $values->daily_limit);

					break;
				}
				case '2': {
					$accountID = $this->clientManager->addNewAccount($clientID, $values->name, $values->account_type, null, $values->interest, null);

					break;
				}
				case '3': {
					$accountID = $this->clientManager->addNewAccount($clientID, $values->name, $values->account_type, $values->amount, $values->interest, null);

					break;
				}
			}

			$this->flashMessage('Nový účet byl úspěšně vytvořen.');

			if ($clientID) {
				$this->redirect("Admin:clientDetail", $clientID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se vytvořit nový účet.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  ADD REGULAR ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	protected function createComponentAddAccountRegularForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Název účtu')
			->setAttribute('placeholder', "Název účtu")
			->setRequired('Musíte zadat název')
			->addRule(Form::MIN_LENGTH, 'Název musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Název může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž\s]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Název účtu');

		$form->addText('daily_limit', 'Denní limit')
			->setAttribute('placeholder', "Denní limit kč")
			->setRequired('Musíte zadat denní limit')
			->addRule(Form::INTEGER, 'Nezadal jste celé číslo')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Denní limit kč');

		$form->addSubmit('send', 'Přidat účet')
		->setAttribute("id", "submit-button");

		$form->addHidden('account_type')
			->setDefaultValue("1");

		$form->onSuccess[] = $this->addAccountRegularFormSucceeded;

		return $form;
	}

	public function addAccountRegularFormSucceeded($form) {
		$this->validateForm($form);
	}

	//  *  *  *  *  *  *  *  *  *  *  *  ADD SAVINGS ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	protected function createComponentAddAccountSavingsForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Název účtu')
			->setAttribute('placeholder', "Název účtu")
			->setRequired('Musíte zadat název')
			->addRule(Form::MIN_LENGTH, 'Název musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Název může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž\s]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Název účtu');

		$form->addText('interest', 'Úrok')
			->setAttribute('placeholder', "Úrok %")
			->setRequired('Musíte zadat úrok')
			->addRule(Form::RANGE, 'Úrok musí být od 0% do 100%', array(0, 100))
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Úrok %');

		$form->addSubmit('send', 'Přidat účet')
		->setAttribute("id", "submit-button");

		$form->addHidden('account_type')
			->setDefaultValue("2");

		$form->onSuccess[] = $this->addAccountSavingsFormSucceeded;

		return $form;
	}

	public function addAccountSavingsFormSucceeded($form) {
		$this->validateForm($form);
	}

	//  *  *  *  *  *  *  *  *  *  *  *  ADD CREDIT ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	protected function createComponentAddAccountCreditForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('name', 'Název účtu')
			->setAttribute('placeholder', "Název účtu")
			->setRequired('Musíte zadat název')
			->addRule(Form::MIN_LENGTH, 'Název musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Název může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž\s]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Název účtu');

		$form->addText('interest', 'Úrok')
			->setAttribute('placeholder', "Úrok %")
			->setRequired('Musíte zadat úrok')
			->addRule(Form::RANGE, 'Úrok musí být od 0% do 100%', array(0, 100))
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Úrok %');

		$form->addText('amount', 'Výše úvěru')
			->setAttribute('placeholder', "Výše úvěru")
			->setRequired('Musíte zadat výši úvěru')
			->addRule(Form::INTEGER, 'Nezadal jste celé číslo')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Výše úvěru');

		$form->addText('type', 'Typ úvěru')
			->setAttribute('placeholder', "Typ úvěru")
			->setRequired('Musíte zadat typ úvěru')
			->addRule(Form::MIN_LENGTH, 'Typ úvěru musí mít alespoň %d znaky', 2)
			->addRule(Form::MAX_LENGTH, 'Typ úvěru může mít maximálně %d znaků', 45)
			->addRule(Form::PATTERN, 'Může obsahovat pouze znaky \'A\' až \'Ž\'', '^[A-Ža-ž\s]+$')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Typ úvěru');

		$form->addSubmit('send', 'Přidat účet')
		->setAttribute("id", "submit-button");

		$form->addHidden('account_type')
			->setDefaultValue("3");

		$form->onSuccess[] = $this->addAccountCreditFormSucceeded;

		return $form;
	}

	public function addAccountCreditFormSucceeded($form) {
		$this->validateForm($form);
	}

//  *  *  *  *  *  *  *  *  *  *  *  ACCOUNT DETAIL  *  *  *  *  *  *  *  *  *  *  *
	public function renderAccountDetail($accountID) {
		$this->template->submenu = "client";

		$account = $this->clientManager->getAccountWithID($accountID);
		$access = $this->accessManager->getAccessByAccountID($accountID);

		if ($account->type == "regular")
			$this->template->detail = "detailAccountRegular";
		else
			$this->template->detail = "detailAccount";

		$accountSpecific = $this->clientManager->getSpecificAccountType($account->type, $account->type_id);

		if ($account != NULL) {
			$access = $this->accessManager->getAccessByAccountID($accountID);

			$this->template->access = $access;

			$count = 0;
			$accessArray = array();

			foreach ($access as $acces) {
				$disponent = $this->clientManager->getClientWithID($acces->client_id);
				$accessArray[$count] = $disponent;
				$count++;
			}

			if ($count != 0)
				$this->template->accessArray = $accessArray;
			
		}

		$this->template->clientID = $account->owner;
		$this->template->accountID = $account->id;

		$this->template->account = $account;
		$this->template->accountSpecific = $accountSpecific;

	}

	public function renderAccount($clientID){
		$this->template->submenu = "client";
		$this->template->detail = "none";

		$this->template->clientID = $clientID;

		$this->template->accounts = $this->clientManager->getAccountsForClient($clientID);
	}

//  *  *  *  *  *  *  *  *  *  *  *  DELETE ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	public function renderDeleteAccount($accountID, $clientID) {
		$this->template->submenu = "client";
		$this->template->clientID = $clientID;

		$client = $this->clientManager->getClientWithID($clientID);

		$this->template->client = $client;

		$account = $this->clientManager->getAccountWithID($accountID);

		$this->template->account = $account;
	}

	public function handleDeleteAccount($accountID, $clientID) {
		$this->beforeValidation();

		$this->clientManager->deleteAccount($accountID);

		$this->flashMessage('Účet byl úspěšně odstraněn.');

		$this->redirect("Admin:clientDetail", $clientID);
	}

	
	//  *  *  *  *  *  *  *  *  *  *  *  EDIT ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	public function renderEditAccount($accountID) {
		$this->template->account = $this->clientManager->getAccountWithID($accountID);;
	}

	protected function createComponentEditAccountForm() {
		$this->loadManagers();

		$form = new Form;

		$accountID = $this->getParameter('accountID');

		$this->loadManagers();
		$account = $this->clientManager->getAccountWithID($accountID);

		$form->addText('daily_limit', 'Denní limit')
			->setAttribute('placeholder', "Denní limit kč")
			->setRequired('Musíte zadat denní limit')
			->addRule(Form::INTEGER, 'Nezadal jste celé číslo')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Denní limit kč');

		$form->addSubmit('send', 'Upravit účet');

		$form->onSuccess[] = $this->editAccountFormSucceeded;

		return $form;
	}

	public function editAccountFormSucceeded($form) {
		$this->beforeValidation();

		$accountID = $this->getParameter('accountID');

		$values = $form->getValues();
		
		try {

			$this->clientManager->updateAccount($accountID, $values->daily_limit);

			$this->flashMessage('Změna účtu byla úspěšně provedena.');

			if ($accountID) {
				$this->redirect("Admin:accountDetail", $accountID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se upravit klienta.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  SEARCH CLIENT  *  *  *  *  *  *  *  *  *  *  *
	public function renderSearchClientByPersonalID($transactionType) {
		$this->template->transactionType = $transactionType;

		$this->template->submenu = "transaction";

		if (!isset($this->template->results)) {
			$this->template->results = NULL;
		}
	}

	protected function createComponentSearchClientByPersonalIDForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('personal_id', 'Číslo občanského průkazu')
			->setAttribute('placeholder', "Číslo občanského průkazu")
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo občanského průkazu');

		$form->addSubmit('send', 'Hledat')
			->setAttribute('id', "search-button")
			->setAttribute('class', "ajax");

		$form->onSuccess[] = $this->searchClientByPersonalIDFormSucceeded;

		return $form;
	}

	public function searchClientByPersonalIDFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$this->template->results = $this->clientManager->searchClientByPersonalID($values->personal_id);

		if ($this->isAjax()) {
			$this->redrawControl('searchResults');
		}
	}

	public function handleShowUserAccounts($clientID) {
		$this->beforeValidation();

		$this->template->accounts = $this->clientManager->getAccountsForClient($clientID);

		$this->template->sharedAccounts = $this->clientManager->getSharedAccountsForClient($clientID);

		$this->template->clientID = $clientID;

		if ($this->isAjax()) {
			$this->redrawControl('accountsSnippet');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  NEW TRANSACTION TRANSFER MONEY  *  *  *  *  *  *  *  *  *  *  *
	public function renderNewTransactionToClient($accountID, $clientID) {
		$this->template->submenu = "transaction";

		$this->template->clietnID = $clientID;
	}

	protected function createComponentNewTransactionToClientForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('variable_symbol', 'Variabilní symbol')
			->setAttribute('placeholder', "Variabilní symbol")
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Variabilní symbol');

		$form->addText('amount', 'Částka')
			->setAttribute('placeholder', "Částka")
			->setRequired('Musíte zadat částku')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Částka');

		$form->addText('account02', 'Účet')
			->setAttribute('placeholder', "Účet")
			->setRequired('Musíte zadat číslo účtu')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Účet');

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->newTransactionToClientFormSucceeded;

		return $form;
	}

	public function newTransactionToClientFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$accountID = $this->getParameter('accountID');
		$clientID = $this->getParameter('clientID');
		
		try {
			$ret = $this->transactionsManager->transactionClientToClient($accountID, $clientID, $values->variable_symbol, $values->amount, $values->account02);

			if ($ret == 1) {
				$this->flashMessage('Zadaný účet neexistuje', 'error');
			} elseif ($ret == 2) {
				$this->flashMessage('Na účtu není dostatečný zůstatek', 'error');
			} else {
				$this->flashMessage('Transakce proběhla v pořádku.');

				$this->redirect("newTransactionToClient", $accountID, $clientID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se provést transakci.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  NEW TRANSACTION COLLECT MONEY  *  *  *  *  *  *  *  *  *  *  *
	public function renderNewTransactionGetMoney($accountID, $clientID) {
		$this->template->submenu = "transaction";

		$this->template->clietnID = $clientID;
	}

	protected function createComponentNewTransactionGetMoneyForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('amount', 'Částka')
			->setAttribute('placeholder', "Částka")
			->setRequired('Musíte zadat částku')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Částka');

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->newTransactionGetMoneyFormSucceeded;

		return $form;
	}

	public function newTransactionGetMoneyFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$accountID = $this->getParameter('accountID');
		$clientID = $this->getParameter('clientID');
		
		try {
			$ret = $this->transactionsManager->transactionGetMoneyFromClient($accountID, $clientID, $values->amount, $this->user->getIdentity()->id);

			if ($ret == 1) {
				$this->flashMessage('Zadaný účet neexistuje', 'error');
			} elseif ($ret == 2) {
				$this->flashMessage('Na účtu není dostatečný zůstatek', 'error');
			} else {
				$this->flashMessage('Transakce proběhla v pořádku.');

				$this->redirect("newTransactionGetMoney", $accountID, $clientID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se provést transakci.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  NEW TRANSACTION DEPOSIT MONEY  *  *  *  *  *  *  *  *  *  *  *
	public function renderNewTransactionGiveMoney($accountID, $clientID) {
		$this->template->submenu = "transaction";

		$this->template->clietnID = $clientID;
	}

	protected function createComponentNewTransactionGiveMoneyForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('amount', 'Částka')
			->setAttribute('placeholder', "Částka")
			->setRequired('Musíte zadat částku')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Částka');

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->newTransactionGiveMoneyFormSucceeded;

		return $form;
	}

	public function newTransactionGiveMoneyFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$accountID = $this->getParameter('accountID');
		$clientID = $this->getParameter('clientID');
		
		try {
			$ret = $this->transactionsManager->transactionGiveMoneyToClient($accountID, $clientID, $values->amount, $this->user->getIdentity()->id);

			if ($ret == 1) {
				$this->flashMessage('Zadaný účet neexistuje', 'error');
			} elseif ($ret == 2) {
				$this->flashMessage('Na účtu není dostatečný zůstatek', 'error');
			} else {
				$this->flashMessage('Transakce proběhla v pořádku.');

				$this->redirect("newTransactionGiveMoney", $accountID, $clientID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se provést transakci.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  ADD ACCESS  *  *  *  *  *  *  *  *  *  *  *
	public function renderAddAccess($accountID, $clientID) {
		$this->template->submenu = "client";
		$this->template->detail = "none";

		$this->template->clientID = $clientID;

		$this->template->accountID = $accountID;
	}

	public function renderAddAccessSetLimit($accountID, $clientID) {
		$this->template->submenu = "client";

		$this->template->clientID = $clientID;

		$this->template->accountID = $accountID;
	}

	protected function createComponentAddAccessSetLimitForm() {
		$this->loadManagers();
		
		$form = new Form;

		$form->addText('limit', 'Limit')
			->setAttribute('placeholder', "Limit")
			->setRequired('Musíte zadat limit')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Limit');

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->addAccessSetLimitFormSucceeded;

		return $form;
	}

	public function addAccessSetLimitFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$accountID = $this->getParameter('accountID');
		$clientID = $this->getParameter('clientID');
		
		try {
			$this->accessManager->addAccessToClient($clientID, $accountID, $values->limit);
			
			$this->flashMessage('Přístup k účtu byl přidán.');

			$this->redirect("accountDetail", $accountID);
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se nastavit přístup.');
		}
	}

	//  *  *  *  *  *  *  *  *  *  *  *  DELETE ACCESS  *  *  *  *  *  *  *  *  *  *  *
	public function renderDeleteAccess($accessID, $clientID) {
		$this->template->accessID = $accessID;

		$access = $this->accessManager->getAccessWithID($accessID);
		$client = $this->clientManager->getClientWithID($clientID);
		$account = $this->clientManager->getAccountWithID($access->account_id);

		$this->template->access = $access;
		$this->template->client = $client;
		$this->template->account = $account;

	}

	public function handleDeleteAccess($accessID, $accountID) {
		$this->beforeValidation();

		$account = $this->clientManager->getAccountWithID($accountID);

		$this->accessManager->deleteAccess($accessID);

		$this->flashMessage('Disponent byl úspěšně odstraněn.');

		$this->redirect("Admin:accountDetail", $account->id);
	}

	public function handleGetAccessId($clientID, $accountID) {
		$this->beforeValidation();

		$access = $this->accessManager->getAccessByClientID($clientID, $accountID);

		$this->redirect("Admin:deleteAccess", $access->id, $clientID);

	}

}