<?php

if (!is_dir(__DIR__ . '/../www/webtemp')){
    mkdir(__DIR__ . '/../www/webtemp');
}

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setDebugMode(TRUE);

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon', Nette\Configurator::AUTO);

$container = $configurator->createContainer();

return $container;
