<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use App\Model\EmployeeManager;
use App\Model\ClientManager;


class CommonPresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context */
    protected $database;

    public $employeeManager;
    public $clientManager;


    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function createComponentUploadImageForm() {
        $form = new Form;

        $form->addUpload('image', 'Obrázek:')
            ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 1,5 MB.', 1500 * 1024 /* v bytech */);

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = $this->uploadImageFormSucceeded;

        return $form;
    }

    public function uploadImageFormSucceeded($form) {
        $this->beforeValidation();

        $values = $form->getValues();
        
        try {
            $file = $values->image;
            $filename = $file->getSanitizedName();
            $imgURLtmp = './images/_tmp/' . $file->name;

            if ($this->user->getIdentity()->role == "client") {
                $imgURL = './images/clients/';
            } else {
                $imgURL = './images/employees/';
            }

            $file->move($imgURLtmp);

            $image = Image::fromFile($imgURLtmp);
            $size = $image->calculateSize(8000, 8000,$image->getWidth(), $image->getHeight());
            $image->crop(0, 0, $size[0], $size[1]);
            $image->resize(400, 400);

            $image->save($imgURL . $this->user->getIdentity()->id . '.jpg', 70, Image::JPEG);

            unlink($imgURLtmp);

            if ($this->user->getIdentity()->role == "client") {
                $clientID = $this->getParameter('clientID');

                $this->clientManager = new ClientManager($this->database);

                $this->clientManager->clientHasImage($this->user->getIdentity()->id);
            } else {
                $employeeID = $this->getParameter('employeeID');

                $this->employeeManager = new EmployeeManager($this->database);

                $this->employeeManager->employeeHasImage($this->user->getIdentity()->id);
            }

            $this->flashMessage('Obrázek byl úspěšně uložen.');
        } catch (\Nette\InvalidStateException $e) {
            $form->addError('Nepodařilo se uložit obrázek.');
        }
    }
}
