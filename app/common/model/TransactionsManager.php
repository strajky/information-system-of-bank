<?php

namespace App\Model;

use Nette;
use App\Model\ClientManager;


/**
 * Users management.
 */
class TransactionsManager extends Nette\Object {
	const
		TABLE_NAME = 'transactions',
		COLUMN_ID = 'id',
		COLUMN_TYPE = 'type',
		COLUMN_VARIABLE_SYMBOL = 'variable_symbol',
		COLUMN_AMOUNT = 'amount',
		COLUMN_DATE = 'date',
		COLUMN_ACCOUNT_01 = 'account01',
		COLUMN_ACCOUNT_02 = 'account02',
		COLUMN_EMPLOYEE_ID = 'employee_id',
		COLUMN_CLIENT_ID = 'client_id';

	/** @var Nette\Database\Context */
	private $database;

	public $clientManager;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	// Z účtu na účet
	public function transactionClientToClient($fromAccountID, $clientID, $variableSymbol, $amount, $toAccountID) {
		try {
			$this->clientManager = new ClientManager($this->database);

			$toAccount = $this->clientManager->getAccountWithID($toAccountID);
			$fromAccount = $this->clientManager->getAccountWithID($fromAccountID);

			if ($toAccount == NULL) {
				return 1;
			} elseif ($fromAccount->balance < $amount) {
				return 2;
			}

			$this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_TYPE => 'from client',
				self::COLUMN_VARIABLE_SYMBOL => $variableSymbol,
				self::COLUMN_AMOUNT => $amount,
				self::COLUMN_DATE => date('Y-m-d G:i:s'),
				self::COLUMN_ACCOUNT_01 => $fromAccountID,
				self::COLUMN_ACCOUNT_02 => $toAccountID,
				self::COLUMN_EMPLOYEE_ID => NULL,
				self::COLUMN_CLIENT_ID => $clientID,
			));

			$this->clientManager->sendAmount($fromAccountID, $amount);
			$this->clientManager->raiseAmount($toAccountID, $amount);

			return 0;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	// Vklad
	public function transactionGetMoneyFromClient($fromAccountID, $clientID, $amount, $employeeID) {
		try {
			$this->clientManager = new ClientManager($this->database);

			$fromAccount = $this->clientManager->getAccountWithID($fromAccountID);

			if ($fromAccountID == NULL) {
				return 1;
			} elseif ($fromAccount->balance < $amount) {
				return 2;
			}

			$this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_TYPE => 'get money',
				self::COLUMN_VARIABLE_SYMBOL => NULL,
				self::COLUMN_AMOUNT => $amount,
				self::COLUMN_DATE => date('Y-m-d G:i:s'),
				self::COLUMN_ACCOUNT_01 => $fromAccountID,
				self::COLUMN_ACCOUNT_02 => NULL,
				self::COLUMN_EMPLOYEE_ID => $employeeID,
				self::COLUMN_CLIENT_ID => $clientID,
			));

			$this->clientManager->sendAmount($fromAccountID, $amount);

			return 0;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	// Výběr
	public function transactionGiveMoneyToClient($fromAccountID, $clientID, $amount, $employeeID) {
		try {
			$this->clientManager = new ClientManager($this->database);

			$fromAccount = $this->clientManager->getAccountWithID($fromAccountID);

			if ($fromAccount == NULL) {
				return 1;
			}

			$this->database->table(self::TABLE_NAME)->insert(array(
				self::COLUMN_TYPE => 'money',
				self::COLUMN_VARIABLE_SYMBOL => NULL,
				self::COLUMN_AMOUNT => $amount,
				self::COLUMN_DATE => date('Y-m-d G:i:s'),
				self::COLUMN_ACCOUNT_01 => $fromAccountID,
				self::COLUMN_ACCOUNT_02 => NULL,
				self::COLUMN_EMPLOYEE_ID => $employeeID,
				self::COLUMN_CLIENT_ID => $clientID,
			));

			$this->clientManager->raiseAmount($fromAccountID, $amount);

			return 0;
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function getTransactionsHistory($accountID) {
		try {
			return $this->database->table(self::TABLE_NAME)->where('account01 = ? OR account02 = ?', $accountID, $accountID)->order('date DESC');
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

/*
	public function updateBranch($id, $adress, $city, $zip, $atm, $description) {
		try {
			$branch = $this->getBranchWithID($id);

			$branch->update(array(
				self::COLUMN_ADRESS => $adress,
				self::COLUMN_CITY => $city,
				self::COLUMN_ZIP => $zip,
				self::COLUMN_ATM => $atm,
				self::COLUMN_EMAIL => $description,
			));
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function deleteBranch($id) {
		try {
			$branch = $this->getBranchWithID($id);
			$branch->delete();
		} catch (\Nette\InvalidStateException $e) {
				$this->flashMessage('Nastala chyba', 'error');
		}
	}

	public function getBranchWithID($branchID) {
		return $this->database->table(self::TABLE_NAME)->get($branchID);
	}

	public function getAllBranches() {
		return $this->database->table(self::TABLE_NAME)->fetchAll();
	}
	*/
}