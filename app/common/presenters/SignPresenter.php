<?php

namespace App\Presenters;

use Nette;
use App\Forms\SignFormFactory;
use Nette\Security\Passwords;


class SignPresenter extends CommonPresenter
{
	/** @var SignFormFactory @inject */
	public $signFormFactory;


	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = $this->signFormFactory->create();
		$form->onSuccess[] = function ($form) {
			if ($this->user->getIdentity()->role == "client") {
				$form->getPresenter()->redirect('Homepage:default');	
			} else {
				$form->getPresenter()->redirect('Admin:');
			}
		};
		return $form;
	}

	public function renderIn() {
		//dump(Passwords::hash("admin"));
	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl(a) jste odhlášen(a)');
		$this->redirect('in');
	}

}
