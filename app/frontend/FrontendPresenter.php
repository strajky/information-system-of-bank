<?php

namespace App\Presenters;

use Nette;


class FrontendPresenter extends CommonPresenter
{
    public function beforeRender()
    {
        parent::beforeRender();
        
        if(!$this->user->isLoggedIn()) 
            $this->redirect('Sign:in');

        if ($this->isAjax()) {
            $this->invalidateControl('flashes');
            //$this->invalidateControl('content');
        }
    }

    public function beforeValidation() {
        if(!$this->user->isLoggedIn())
            $this->redirect('Sign:in');
    }
}
