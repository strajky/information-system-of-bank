<?php

namespace App\Presenters;

use Nette;
use App\Model\Settings;
use App\Model\EmployeeManager;
use App\Model\BranchManager;
use App\Model\ClientManager;
use App\Model\AccessManager;
use App\Model\TransactionsManager;
use Nette\Application\UI\Form;


class HomepagePresenter extends FrontendPresenter
{
	public $employeeManager;
	public $branchManager;
	public $clientManager;
	public $accessManager;
	public $transactionsManager;

	public function beforeRender() {
		parent::beforeRender();

		if ($this->user->getIdentity()->role != "client") {
			$this->getPresenter()->redirect('Admin:');
		}

		$this->loadManagers();		

		$this->template->role = $this->user->getIdentity()->role;
		$this->template->userName = $this->user->getIdentity()->name;
		$this->template->clientID = $this->user->getIdentity()->id;
		$this->template->hasImage = $this->clientManager->hasImage($this->user->getIdentity()->id);
	}

	public function beforeValidation() {
		parent::beforeValidation();

		$this->loadManagers();
	}

	protected function loadManagers() {
		$this->clientManager = new ClientManager($this->database);
		$this->accessManager = new AccessManager($this->database);
		$this->transactionsManager = new TransactionsManager($this->database);
	}

	public function renderDefault() {
		$this->template->accounts = $this->clientManager->getAccountsForClient($this->user->getIdentity()->id);

		$this->template->sharedAccounts = $this->clientManager->getSharedAccountsForClient($this->user->getIdentity()->id);
	}

	public function renderAccountDetail($accountID) {
		$this->template->accountID = $accountID;

		$account = $this->clientManager->getAccountWithID($accountID);

		$accountSpecific = $this->clientManager->getSpecificAccountType($account->type, $account->type_id);

		if (($account != NULL) && ($account->owner == $this->user->getIdentity()->id)) {
			$access = $this->accessManager->getAccessByAccountID($accountID);

			$this->template->access = $access;

			$count = 0;
			$accessArray = array();

			foreach ($access as $acces) {
				$disponent = $this->clientManager->getClientWithID($acces->client_id);
				$accessArray[$count] = $disponent;
				$count++;
			}

			$this->template->accessArray = $accessArray;
		}

		$this->template->clientID = $this->user->getIdentity()->id;
		$this->template->accountID = $account->id;

		$this->template->account = $account;
		$this->template->accountSpecific = $accountSpecific;
	}

	public function renderNewTransaction($accountID) {
		$this->template->accountID = $accountID;

		$this->template->account = $this->clientManager->getAccountWithID($accountID);
	}

	public function renderuploadImage($accountID) {

	}

	protected function createComponentNewTransactionForm() {
		$this->loadManagers();

		$form = new Form;

		$form->addText('variable_symbol', 'Variabilní symbol')
			->setAttribute('placeholder', "Variabilní symbol*")
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Variabilní symbol*');

		$form->addText('amount', 'Částka')
			->setAttribute('placeholder', "Částka Kč*")
			->addRule(Form::RANGE, 'Častka musí být větší než 20 Kč', array(20, 9999999))
			->setRequired('Musíte zadat částku v Kč')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Částka kč*');

		$form->addText('account02', 'Číslo bankovního účtu')
			->setAttribute('placeholder', "Číslo bankovního účtu*")
			->setRequired('Musíte zadat číslo bankovního účtu')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Číslo bankovního účtu*');

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->newTransactionFormSucceeded;

		return $form;
	}

	public function newTransactionFormSucceeded($form) {
		$this->beforeValidation();

		$values = $form->getValues();

		$accountID = $this->getParameter('accountID');
		
		try {
			$ret = $this->transactionsManager->transactionClientToClient($accountID, $this->user->getIdentity()->id, $values->variable_symbol, $values->amount, $values->account02);

			if ($ret == 1) {
				$this->flashMessage('Zadaný účet neexistuje', 'error');
			} elseif ($ret == 2) {
				$this->flashMessage('Na účtu není dostatečný zůstatek', 'error');
			} else {
				$this->flashMessage('Transakce proběhla v pořádku.');

				$this->redirect("accountDetail", $accountID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se provést transakci.');
		}
	}

	public function renderClientDetail() {
		$this->template->submenu = "client_options";

		$this->template->client = $this->clientManager->getClientWithID($this->user->getIdentity()->id);

		$profileImage = $this->template->client->username;
	}

	//  *  *  *  *  *  *  *  *  *  *  *  CHANGE PASSWORD  *  *  *  *  *  *  *  *  *  *  *
	public function createComponentChangePasswordForm() {
		$this->loadManagers();

    	$form = new Form;

		$form->addPassword('oldPassword', '')
		    ->addRule(Form::FILLED, 'Zadejte Vaše staré heslo')
		    ->setAttribute('placeholder', 'Původní heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Staré heslo');
		$form->addPassword('newPassword', '')
		    ->addRule(Form::FILLED, 'Zadejte nové heslo')
		    ->setAttribute('placeholder', 'Nové heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Nové heslo');
		$form->addPassword('newPassword2', '')
		    ->addRule(Form::FILLED, 'Znovu zadejte nové heslo')
		    ->addRule(Form::EQUAL, "Nové heslo se neshoduje", $form["newPassword"])
		    ->setAttribute('placeholder', 'Znovu nové heslo')
		    ->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Nové heslo');

    	$form->addSubmit('send', 'Změnit');

    	$form->onSuccess[] = $this->processChangePasswordForm;

    	return $form;
    }

    public function processChangePasswordForm($form) {
    	$this->loadManagers();
    	$this->template->submenu = "client_options";

        $values = $form->getValues();

        $userId = $this->getUser()->id;
        $oldPassword = $values["oldPassword"];
        $newPassword = $values["newPassword"];

        try {
        	$ret = $this->clientManager->changePassword($userId, $oldPassword, $newPassword);

            if ($ret == 0) {
            	$this->flashMessage('Heslo bylo úspěšně změněno.');
            	$this->redirect('Homepage:clientDetail');
            } else if ($ret == 1) {
            	$this->flashMessage('Původní heslo je chybné.');
            }
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Změna hesla se nezdařila.');
        }

    }

    public function renderTransactionHistory($accountID) {
    	$this->template->account = $this->clientManager->getAccountWithID($accountID);

    	$this->template->accountID = $accountID;
    	$this->template->transactions = $this->transactionsManager->getTransactionsHistory($accountID);
	}

	//  *  *  *  *  *  *  *  *  *  *  *  EDIT ACCOUNT  *  *  *  *  *  *  *  *  *  *  *
	public function renderEditAccount($accountID) {
		$this->template->accountID = $accountID;

		$this->template->account = $this->clientManager->getAccountWithID($accountID);;
	}

	protected function createComponentEditAccountForm() {
		$this->loadManagers();

		$form = new Form;

		$accountID = $this->getParameter('accountID');

		$this->loadManagers();
		$account = $this->clientManager->getAccountWithID($accountID);

		$form->addText('daily_limit', 'Denní limit')
			->setAttribute('placeholder', "Denní limit Kč")
			->setRequired('Musíte zadat denní limit')
			->addRule(Form::INTEGER, 'Nezadal jste celé číslo')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Denní limit kč');

		$form->addSubmit('send', 'Upravit účet');

		$form->onSuccess[] = $this->editAccountFormSucceeded;

		return $form;
	}

	public function editAccountFormSucceeded($form) {
		$this->beforeValidation();

		$accountID = $this->getParameter('accountID');

		$values = $form->getValues();
		
		try {

			$this->clientManager->updateAccount($accountID, $values->daily_limit);

			$this->flashMessage('Změna účtu byla úspěšně provedena.');

			if ($accountID) {
				$this->redirect("Homepage:accountDetail", $accountID);
			}
		} catch (\Nette\InvalidStateException $e) {
			$form->addError('Nepodařilo se upravit klienta.');
		}
	}
}
