-- phpMyAdmin SQL Dump
-- version 4.4.4
-- http://www.phpmyadmin.net
--
-- Počítač: innodb.endora.cz:3306
-- Vytvořeno: Čtv 10. pro 2015, 23:59
-- Verze serveru: 5.5.38-35.2-log
-- Verze PHP: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `bank`
--
CREATE DATABASE IF NOT EXISTS `bank` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bank`;

-- --------------------------------------------------------

--
-- Struktura tabulky `access`
--

DROP TABLE IF EXISTS `access`;
CREATE TABLE IF NOT EXISTS `access` (
  `client_id` int(11) NOT NULL,
  `account_id` int(9) NOT NULL,
  `limit` int(20) NOT NULL,
  `id` int(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `access`
--

INSERT INTO `access` (`client_id`, `account_id`, `limit`, `id`) VALUES
(6, 189145082, 15000, 1),
(7, 189145090, 30000, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(9) NOT NULL,
  `balance` int(20) NOT NULL,
  `date_created` date NOT NULL,
  `owner` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(10) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=189145091 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `account`
--

INSERT INTO `account` (`id`, `balance`, `date_created`, `owner`, `name`, `type`, `type_id`) VALUES
(189145082, 13336959, '2015-12-06', 5, 'Manželšký', 'regular', 5),
(189145083, 10000, '2015-12-06', 5, 'Na auto', 'savings', 3),
(189145084, 7500, '2015-12-06', 5, 'Dům', 'credit', 3),
(189145085, 35990, '2015-12-06', 6, 'Běžný', 'regular', 6),
(189145086, 122200, '2015-12-06', 4, 'Můj účet', 'regular', 7),
(189145087, 117877, '2015-12-10', 7, 'Manželský', 'regular', 8),
(189145088, 345900, '2015-12-10', 7, 'Na auto', 'savings', 4),
(189145089, 294560, '2015-12-10', 7, 'Rodinný Dům', 'credit', 4),
(189145090, 13456789, '2015-12-10', 8, 'CG Transit', 'regular', 9);

-- --------------------------------------------------------

--
-- Struktura tabulky `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL,
  `adress` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip` int(5) NOT NULL,
  `atm` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `manager` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `branch`
--

INSERT INTO `branch` (`id`, `adress`, `city`, `zip`, `atm`, `description`, `manager`) VALUES
(5, 'Václavská 11', 'Brno', 61800, 1, 'Pobočka se nachází na ulici Václavská 11. Jedná se o centrální pobočku v Brně. Otevřeno Po-Pá 8:00-17:00.', 68),
(6, 'Stará 24', 'Brno', 61808, 1, 'Pobočka se nachází na ulici Stará 24. Menší pobočka s plnohodnotnými slušbami. Otevřeno Po-Pá 8:00-17:00.', NULL),
(7, 'Cejcká 6', 'Brno', 61812, 0, 'Pobočka se nachází na ulici Cejcká 6. Menší pobočka s plnohodnotnými slušbami. Otevřeno Po-Pá 8:00-17:00.', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL,
  `personal_id` varchar(10) NOT NULL,
  `role` varchar(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `personal_number` varchar(12) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `birthday_number` varchar(10) NOT NULL,
  `adress` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip` int(5) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `has_image` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `clients`
--

INSERT INTO `clients` (`id`, `personal_id`, `role`, `username`, `password`, `personal_number`, `name`, `surname`, `sex`, `birthday_number`, `adress`, `city`, `zip`, `phone`, `email`, `has_image`) VALUES
(4, '981000171', 'client', 'xnekon01', '$2y$10$bdH18zYKFNCRe0IDMUgeZ.7RBO2VEym7YA2kHjFzN3FfSM6n1NA5y', '661201/0009', 'Honza', 'Nekončný', 'muž', '1.12.1966', 'Fostrova 23', 'Olomouc', 61302, '+420725100789', 'honzanekonecny@mail.it', 1),
(5, '912001345', 'client', 'xdvors01', '$2y$10$J.EB6LIcJTZueW/omiR74uNyJ0RXjQus3MHzawrVgpoAOBBWfYQwm', '751209/0564', 'Jana', 'Dvorská', 'muž', '9.12.1975', 'Holzova 43', 'Brno', 61208, '+420608765482', 'janca65@gmejl.com', 1),
(6, '976439012', 'client', 'xdvors02', '$2y$10$mVhtR6xfyeSBrBlVqscNZuWdSVKG5rpUArLlUzUwwGxQcExLPwFe2', '711209/0568', 'Lukáš', 'Dvorský', 'muž', '9.12.1971', 'Holzova 43', 'Brno', 61700, '+420607654738', 'lukasdvorsky@gmejl.com', 1),
(7, '991480823', 'client', 'xzkusj01', '$2y$10$mVhtR6xfyeSBrBlVqscNZuWdSVKG5rpUArLlUzUwwGxQcExLPwFe2', '720806/2213', 'Jan', 'Zkus', 'muž', '6.8.1972', 'Nerudova 12', 'Brno', 62800, '+420725178908', 'dostacny@gmejl.com', 1),
(8, '930001987', 'client', 'xtokov01', '$2y$10$mVhtR6xfyeSBrBlVqscNZuWdSVKG5rpUArLlUzUwwGxQcExLPwFe2', '765623/1209', 'Eva', 'Toková', 'žena', '23.6.1976', 'Brněnská 11', 'Brno', 62805, '+420722100989', 'eva.tokova@cgtransti.de', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `credit_account`
--

DROP TABLE IF EXISTS `credit_account`;
CREATE TABLE IF NOT EXISTS `credit_account` (
  `id` int(11) NOT NULL,
  `amount` int(20) NOT NULL,
  `interest` int(3) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `credit_account`
--

INSERT INTO `credit_account` (`id`, `amount`, `interest`, `type`) VALUES
(3, 1200000, 5, ''),
(4, 1200000, 3, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` varchar(20) NOT NULL,
  `personal_number` varchar(12) NOT NULL,
  `personal_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(30) NOT NULL,
  `adress` varchar(45) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `birthday_number` varchar(10) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(45) NOT NULL,
  `branch` int(11) DEFAULT NULL,
  `has_image` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `employee`
--

INSERT INTO `employee` (`id`, `username`, `password`, `role`, `personal_number`, `personal_id`, `name`, `adress`, `surname`, `sex`, `birthday_number`, `city`, `zip`, `phone`, `email`, `branch`, `has_image`) VALUES
(68, 'admin', '$2y$10$jCwbWo22G0vbLbMnsMD.UelymwIZtnkJnV8k3HDEhMUH6ks2OG3wC', 'admin', '870721/3240', '998000398', 'Lukáš', 'Brněnská 23', 'Král', 'muž', '21.7.1987', 'Brno', 62805, '+420725777100', 'lukaskral@bank.cz', NULL, 1),
(71, 'xstrun01', '$2y$10$WLl9EZ5VqAMaM8v1PqZAduRRte6Ak7TyXlB3wCesaexQOXldsLHyW', 'employee', '831013/0565', '997000823', 'David', 'Molákova 11', 'Štrunc', 'muž', '13.10.1983', 'Brno', 62812, '+420777100000', 'adam.konecny94@gmail.com', 5, 1),
(72, 'xmikus01', '$2y$10$Hwr8rBYc66.ErQONPNb23ODlpEeA4IKoFu.6XUW3wi5hw.JaV26Iq', 'employee', '930806/4007', '997000171', 'Martin', 'Brněnská 11', 'Mikušíková', 'muž', '6.8.1993', 'Brno', 61800, '+420604567908', 'martinamikus@gmejl.com', 6, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `regular_account`
--

DROP TABLE IF EXISTS `regular_account`;
CREATE TABLE IF NOT EXISTS `regular_account` (
  `id` int(11) NOT NULL,
  `daily_limit` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `regular_account`
--

INSERT INTO `regular_account` (`id`, `daily_limit`) VALUES
(5, 20000),
(6, 100000),
(7, 13000),
(8, 20),
(9, 20);

-- --------------------------------------------------------

--
-- Struktura tabulky `savings_account`
--

DROP TABLE IF EXISTS `savings_account`;
CREATE TABLE IF NOT EXISTS `savings_account` (
  `id` int(11) NOT NULL,
  `interest` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `savings_account`
--

INSERT INTO `savings_account` (`id`, `interest`) VALUES
(3, 2),
(4, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `variable_symbol` int(10) DEFAULT NULL,
  `amount` int(20) NOT NULL,
  `date` date NOT NULL,
  `account01` int(9) NOT NULL,
  `account02` int(9) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `transactions`
--

INSERT INTO `transactions` (`id`, `type`, `variable_symbol`, `amount`, `date`, `account01`, `account02`, `employee_id`, `client_id`) VALUES
(0, 'money', NULL, 23567, '2015-12-06', 189145082, NULL, 72, 5),
(0, 'money', NULL, 12098, '2015-12-06', 189145082, NULL, 72, 5),
(0, 'money', NULL, 33300, '2015-12-06', 189145082, NULL, 72, 5),
(0, 'get money', NULL, 2300, '2015-12-06', 189145082, NULL, 72, 5),
(0, 'money', NULL, 9770, '2015-12-06', 189145082, NULL, 72, 6),
(0, 'money', NULL, 120000, '2015-12-06', 189145086, NULL, 72, 4),
(0, 'from client', 9765, 1200, '2015-12-06', 189145082, 189145086, NULL, 6),
(0, 'from client', 9765, 1000, '2015-12-06', 189145082, 189145086, NULL, 6),
(0, 'from client', 1095, 10000, '2015-12-06', 189145082, 189145083, NULL, 5),
(0, 'from client', 1765, 12000, '2015-12-06', 189145082, 189145084, NULL, 5),
(0, 'from client', 1090, 4500, '2015-12-06', 189145084, 189145082, NULL, 5),
(0, 'from client', 9, 35990, '2015-12-06', 189145082, 189145085, NULL, 6),
(0, 'money', NULL, 1345632, '2015-12-10', 189145087, NULL, 72, 7),
(0, 'money', NULL, 345900, '2015-12-10', 189145088, NULL, 72, 7),
(0, 'money', NULL, 1105677, '2015-12-10', 189145089, NULL, 72, 7),
(0, 'money', NULL, 10856782, '2015-12-10', 189145090, NULL, 72, 8),
(0, 'from client', 456, 12350, '2015-12-10', 189145087, 189145090, NULL, 7),
(0, 'from client', 939378, 150000, '2015-12-10', 189145087, 189145082, NULL, 7),
(0, 'from client', 158, 1183282, '2015-12-10', 189145087, 189145082, NULL, 7),
(0, 'from client', 0, 99999, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 0, 99999, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 0, 900000, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 0, 5090, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 0, 589, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 0, 1086913, '2015-12-10', 189145090, 189145082, NULL, 7),
(0, 'from client', 0, 9782219, '2015-12-10', 189145090, 189145082, NULL, 7),
(0, 'money', NULL, 120000, '2015-12-10', 189145087, NULL, 72, 7),
(0, 'money', NULL, 13456789, '2015-12-10', 189145090, NULL, 72, 8),
(0, 'money', NULL, 300560, '2015-12-10', 189145089, NULL, 72, 7),
(0, 'from client', 555, 1000, '2015-12-10', 189145087, 189145082, NULL, 7),
(0, 'from client', 666, 123, '2015-12-10', 189145087, 189145082, NULL, 7),
(0, 'from client', 0, 5000, '2015-12-10', 189145089, 189145082, NULL, 7),
(0, 'from client', 11, 1000, '2015-12-10', 189145087, 189145082, NULL, 7),
(0, 'from client', 0, 1000, '2015-12-10', 189145089, 189145082, NULL, 7);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Klíče pro tabulku `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- Klíče pro tabulku `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manager` (`manager`);

--
-- Klíče pro tabulku `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_id` (`personal_id`),
  ADD UNIQUE KEY `personal_number` (`personal_number`);

--
-- Klíče pro tabulku `credit_account`
--
ALTER TABLE `credit_account`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_number` (`personal_number`),
  ADD UNIQUE KEY `personal_id` (`personal_id`),
  ADD KEY `branch` (`branch`);

--
-- Klíče pro tabulku `regular_account`
--
ALTER TABLE `regular_account`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `savings_account`
--
ALTER TABLE `savings_account`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `transactions`
--
ALTER TABLE `transactions`
  ADD KEY `account02` (`account02`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `account01` (`account01`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `access`
--
ALTER TABLE `access`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `account`
--
ALTER TABLE `account`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=189145091;
--
-- AUTO_INCREMENT pro tabulku `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pro tabulku `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pro tabulku `credit_account`
--
ALTER TABLE `credit_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pro tabulku `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT pro tabulku `regular_account`
--
ALTER TABLE `regular_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pro tabulku `savings_account`
--
ALTER TABLE `savings_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `access_ibfk_3` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `access_ibfk_4` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `branch`
--
ALTER TABLE `branch`
  ADD CONSTRAINT `branch_ibfk_3` FOREIGN KEY (`manager`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`branch`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`account02`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_5` FOREIGN KEY (`account01`) REFERENCES `account` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
