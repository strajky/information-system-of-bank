<?php
// source: /Users/strajky/SourceTree/is_banky/banka/app/backend/admin/presenters/templates/Admin/default.latte

class Templatee2eb4dba630ba7c5ccc685d6ac2e5222 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('41c6bbc325', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb7425bdbd5a_content')) { function _lb7425bdbd5a_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;if ($employee->has_image == 1) { ?>
	<img src="../images/employees/<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($employee->id), ENT_COMPAT) ?>.jpg" class="profile-photo-big">
<?php } else { ?>
	<img src="../images/default_profile_color.svg" class="profile-photo-big">
<?php } ?>

<h1><?php echo Latte\Runtime\Filters::escapeHtml($name, ENT_NOQUOTES) ?> <?php echo Latte\Runtime\Filters::escapeHtml($surname, ENT_NOQUOTES) ?></h1>

<table class="table table-hover user_detail">
	<tbody>
		<tr>
			<th>Pozice</th>
			<td>
				<?php if ($employee->role == "admin") { ?>Administrátor<?php } else { ?>Zaměstananec na pobočce<?php } ?>

			</td>
		</tr>
		<tr>
			<th>Uživatelské jméno</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->username, ENT_NOQUOTES) ?></td>
		<tr>
			<th>Rodné číslo</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->personal_number, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Číslo občanského průkazu</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->personal_id, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Datum narození</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->birthday_number, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Pohlaví</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->sex, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Adresa</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->adress, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Město</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->city, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>PSČ</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->zip, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Telefon</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->phone, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>	
			<th>E-mail</th>
			<td><?php echo Latte\Runtime\Filters::escapeHtml($employee->email, ENT_NOQUOTES) ?></td>
		</tr>
		<tr>
			<th>Pobočka
			<td><?php if ($employee->branch) { ?>

					<a title="Zobrazit pobočku" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:branchDetail", array($employee->branch)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($branch->adress, ENT_NOQUOTES) ?>
, <?php echo Latte\Runtime\Filters::escapeHtml($branch->city, ENT_NOQUOTES) ?></a>
<?php } ?>
			</td>
		</tr>
	</tbody>
</table>



<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = '../../../../@layout.latte'; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
// ?>


<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}