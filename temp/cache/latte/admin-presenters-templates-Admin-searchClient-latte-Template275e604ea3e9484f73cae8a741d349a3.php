<?php
// source: /Users/strajky/SourceTree/is_banky/banka/app/backend/admin/presenters/templates/Admin/searchClient.latte

class Template275e604ea3e9484f73cae8a741d349a3 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('cba3d9abad', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbe288d6af72_content')) { function _lbe288d6af72_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><h1>Vyhledat klienta v databázi</h1>

<?php $_l->tmp = $_control->getComponent("searchClientForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>

<div id="<?php echo $_control->getSnippetId('searchResults') ?>"><?php call_user_func(reset($_b->blocks['_searchResults']), $_b, $template->getParameters()) ?>
</div>
<script>
	$('#frm-searchClientForm-name').keypress(function () {
		if ($(this).val().length > 0) {

			$('#search-button').trigger("click");
		}
	});
</script>

<style>
	#search-button {
		display: none;
	}
</style>

<?php
}}

//
// block _searchResults
//
if (!function_exists($_b->blocks['_searchResults'][] = '_lb5305408429__searchResults')) { function _lb5305408429__searchResults($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('searchResults', FALSE)
;if ($results) { ?>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Jméno</th>
					<th>Příjmení</th>
					<th>Adresa</th>
					<th>Město</th>
					<th>PSČ</th>
					<th>Telefon</th>
					<th>Email</th>
				</tr>
			</thead>

			<tbody>
<?php $iterations = 0; foreach ($results as $result) { ?>
				<!-- <tr class="clickable-row" n:href="Admin:clientDetail $result->id"> -->
					<tr>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->name, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->surname, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->adress, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->city, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->zip, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->phone, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->email, ENT_NOQUOTES) ?></a></td>
					</tr>
<?php $iterations++; } ?>
			</tbody>
		</table>
<?php } 
}}

//
// end of blocks
//

// template extending

$_l->extends = '../../../../@layout.latte'; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
// ?>


<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}