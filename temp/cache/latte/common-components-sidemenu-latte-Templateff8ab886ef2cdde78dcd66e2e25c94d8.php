<?php
// source: /Users/strajky/SourceTree/is_banky/banka/app/backend/admin/presenters/templates/Admin/../../../../../common/components/sidemenu.latte

class Templateff8ab886ef2cdde78dcd66e2e25c94d8 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('9009a6e8be', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block sidemenu
//
if (!function_exists($_b->blocks['sidemenu'][] = '_lb10387598be_sidemenu')) { function _lb10387598be_sidemenu($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;if (isset($role)) { if ($role == "admin") { ?>
		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:addEmployee")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:addEmployee"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/addUser.svg">
		    <p>Přidat<br>zaměstnance</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:addBranch")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:addBranch"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/addBranch.svg">
		    <p>Přidat pobočku</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:searchEmployee")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchEmployee"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/findUser.svg">
		    <p>Vyhledat zaměstnance</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:branches")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:branches"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/findBranch.svg">
		    <p>Pobočky</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:manageTime")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:manageTime"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/time.svg">
		    <p>Správa času</p>
		</a>
<?php } elseif ($role == "employee") { ?>
		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:addClient")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:addClient"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/addUser.svg">
		    <p>Vytvořit klienta</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:searchClient")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchClient"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/findUser.svg">
		    <p>Vyhledat klienta</p>
		</a>

		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:searchClientByPersonalID", array(1))) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchClientByPersonalID", array(1)), ENT_COMPAT) ?>
">
		    <img src="../images/menu/transaction.svg">
		    <p>Transakce</p>
		</a>
<?php } elseif ($role == "client") { ?>
		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Homepage:")) { ?>active-menu<?php } ?>
" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:"), ENT_COMPAT) ?>
">
		    <img src="../images/menu/account.svg">
		    <p>Bankovní účty</p>
		</a>

<?php if (isset($account) && ($account->type != "savings")) { ?>
			<a class="menu-item <?php if ($_presenter->isLinkCurrent("Homepage:newTransaction")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:newTransaction", array($account->id)), ENT_COMPAT) ?>
">
			    <img src="../images/menu/transaction.svg">
			    <p>Transakce</p>
			</a>

			<a class="menu-item <?php if ($_presenter->isLinkCurrent("Homepage:transactionHistory")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:transactionHistory", array($accountID)), ENT_COMPAT) ?>
">
			    <img class="history_transaction_img" src="../images/menu/history_transaction.svg">
			    <p>Historie Transakcí</p>
			</a>

<?php if ($account->type == "regular") { ?>
				<a class="menu-item <?php if ($_presenter->isLinkCurrent("Homepage:editAccount")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:editAccount", array($account->id)), ENT_COMPAT) ?>
">
				    <img src="../images/menu/settings.svg">
				    <p>Nastavení účtů</p>
				</a>
<?php } } } ?>

<?php if ($role == "client") { ?>
		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Homepage:clientDetail")) { ?>
active-menu<?php } ?>" id="menu-item-profile" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:clientDetail"), ENT_COMPAT) ?>
">
<?php if ($hasImage == 1) { ?>
				<img src="../images/clients/<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($clientID), ENT_COMPAT) ?>.jpg" class="profile-photo">
<?php } else { ?>
				<img src="../images/default_profile.svg" class="profile-photo">
<?php } ?>

			<p><?php echo Latte\Runtime\Filters::escapeHtml($userName, ENT_NOQUOTES) ?></p>
		</a>
<?php } else { ?>
		<a class="menu-item <?php if ($_presenter->isLinkCurrent("Admin:")) { ?>active-menu<?php } ?>
" id="menu-item-profile" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:"), ENT_COMPAT) ?>
">
<?php if ($hasImage == 1) { ?>
				<img src="../images/employees/<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($employeeID), ENT_COMPAT) ?>.jpg" class="profile-photo">
<?php } else { ?>
				<img src="../images/default_profile.svg" class="profile-photo">
<?php } ?>

			<p><?php echo Latte\Runtime\Filters::escapeHtml($userName, ENT_NOQUOTES) ?></p>
		</a>
<?php } } ?>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>

<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['sidemenu']), $_b, get_defined_vars()) ; 
}}