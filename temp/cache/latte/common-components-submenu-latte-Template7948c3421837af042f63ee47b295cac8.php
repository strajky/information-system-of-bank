<?php
// source: /Users/strajky/SourceTree/is_banky/banka/app/backend/admin/presenters/templates/Admin/../../../../../common/components/submenu.latte

class Template7948c3421837af042f63ee47b295cac8 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('e0e0145686', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block submenu
//
if (!function_exists($_b->blocks['submenu'][] = '_lba7078408b1_submenu')) { function _lba7078408b1_submenu($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;if (isset($submenu)) { if ($submenu == "employee") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:employeeDetail")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:employeeDetail", array($employeeID)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/info.svg"> Přehled
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:editEmployee")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:editEmployee", array($employeeID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/editUser.svg"> Editovat zaměstance
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:deleteEmployee")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:deleteEmployee", array($employeeID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/deleteUser.svg"> Odstranit zaměstance
		</a>
<?php } elseif ($submenu == "branch") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:branchDetail")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:branchDetail", array($branchID)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/info.svg"> Přehled
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:editBranch")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:editBranch", array($branchID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/editBranch.svg"> Editovat pobočku
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:deleteBranch")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:deleteBranch", array($branchID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/deleteBranch.svg"> Odstranit pobočku
		</a>
<?php } elseif ($submenu == "user") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:changePassword")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:changePassword"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/password.svg"> Změna hesla
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:uploadImage")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:uploadImage"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/image.svg"> Změna obrázku
		</a>

		<a class="submenu-item" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Sign:out"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/logout.svg"> Odhlásit se
		</a>
<?php } elseif ($submenu == "client") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:clientDetail")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:clientDetail", array($clientID)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/info.svg"> Přehled
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:editClient")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:editClient", array($clientID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/editUser.svg"> Editovat klienta
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:deleteClient")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:deleteClient", array($clientID)), ENT_COMPAT) ?>
">
			<img src="../images/menu/deleteUser.svg"> Odstranit klienta
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:addAccount")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:addAccount", array($clientID)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/addAccount.svg"> Přidat účet
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:account")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:account", array($clientID)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/deleteAccount.svg"> Odstranit účet
		</a>
<?php if ($detail == "detailAccountRegular") { ?>
			<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:editAccount")) { ?>
active-menu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:editAccount", array($account->id)), ENT_COMPAT) ?>
">
				<img src="../images/submenu/settings.svg"> Nastavení účtů
			</a>
<?php } ?>

<?php } elseif ($submenu == "transaction") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:searchClientByPersonalID", array(1))) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchClientByPersonalID", array(1)), ENT_COMPAT) ?>
">
			<img src="../images/menu/transfer_money.svg"> Bezhotovostní operace
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:searchClientByPersonalID", array(2))) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchClientByPersonalID", array(2)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/collectMoney.svg"> Vybrat z účtu
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Admin:searchClientByPersonalID", array(3))) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:searchClientByPersonalID", array(3)), ENT_COMPAT) ?>
">
			<img src="../images/submenu/depositMoney.svg"> Vložit na účet
		</a>
<?php } elseif ($submenu == "client_options") { ?>
		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Homepage:changePassword")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:changePassword"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/password.svg"> Změna hesla
		</a>

		<a class="submenu-item <?php if ($_presenter->isLinkCurrent("Homepage:uploadImage")) { ?>
active-submenu<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:uploadImage"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/image.svg"> Změna obrázku
		</a>

		<a class="submenu-item" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Sign:out"), ENT_COMPAT) ?>
">
			<img src="../images/submenu/logout.svg"> Odhlásit se
		</a>
<?php } } ?>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>

<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['submenu']), $_b, get_defined_vars()) ; 
}}