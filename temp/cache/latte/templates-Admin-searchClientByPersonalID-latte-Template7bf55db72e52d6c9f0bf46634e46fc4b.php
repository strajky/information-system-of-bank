<?php
// source: /Users/strajky/SourceTree/is_banky/banka/app/backend/admin/presenters/templates/Admin/searchClientByPersonalID.latte

class Template7bf55db72e52d6c9f0bf46634e46fc4b extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('16bbb7c98d', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb648d1691cc_content')) { function _lb648d1691cc_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><h1>Vyhledat klienta podle čísla občanského průkazu</h1>

<?php $_l->tmp = $_control->getComponent("searchClientByPersonalIDForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>

<div id="<?php echo $_control->getSnippetId('searchResults') ?>"><?php call_user_func(reset($_b->blocks['_searchResults']), $_b, $template->getParameters()) ?>
</div>
<div id="<?php echo $_control->getSnippetId('accountsSnippet') ?>"><?php call_user_func(reset($_b->blocks['_accountsSnippet']), $_b, $template->getParameters()) ?>
</div>
<script>
	$('#frm-searchClientByPersonalIDForm-personal_id').keypress(function () {
		if ($(this).val().length > 0) {

			$('#search-button').trigger("click");
		}
	});
</script>

<style>
	#search-button {
		display: none;
	}
</style>

<?php
}}

//
// block _searchResults
//
if (!function_exists($_b->blocks['_searchResults'][] = '_lbad39d0a450__searchResults')) { function _lbad39d0a450__searchResults($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('searchResults', FALSE)
;if ($results) { ?>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Jméno</th>
					<th>Příjmení</th>
					<th>Adresa</th>
					<th>Město</th>
					<th>PSČ</th>
					<th>Telefon</th>
					<th>Email</th>
				</tr>
			</thead>

			<tbody>
<?php $iterations = 0; foreach ($results as $result) { ?>
				<!-- <tr class="clickable-row" n:href="Admin:clientDetail $result->id"> -->
					<tr>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->name, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->surname, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->adress, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->city, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->zip, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->phone, ENT_NOQUOTES) ?></a></td>
						<td><a class="table-row search-row ajax" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("showUserAccounts!", array($result->id)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($result->email, ENT_NOQUOTES) ?></a></td>
					</tr>
<?php $iterations++; } ?>
			</tbody>
		</table>
<?php } 
}}

//
// block _accountsSnippet
//
if (!function_exists($_b->blocks['_accountsSnippet'][] = '_lb25abe2eb9b__accountsSnippet')) { function _lb25abe2eb9b__accountsSnippet($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('accountsSnippet', FALSE)
;if (isset($accounts)) { ?>
		<br>
		<h3>Vlastní účty</h3>

		<table class="table table-hover user_detail account_access">
			<thead>
				<tr>
					<th>Číslo účtu</th>
					<th>Název účtu</th>
					<th>Zůstatek</th>
					<th>Typ účtu</th>
				</tr>
			</thead>
			<tbody>
<?php $iterations = 0; foreach ($accounts as $account) { ?>
					<tr>
<?php if ($transactionType == 1) { if ($account->type != "savings") { ?>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
">
									<?php if ($account->type == "regular") { ?>Běžný účet
									<?php } else { ?>Úvěrový účet<?php } ?>

								</a></td>
<?php } else { ?>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></td>
								<td>Spořící účet</td>
<?php } } elseif ($transactionType == 2) { if ($account->type != "savings") { ?>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
">
									<?php if ($account->type == "regular") { ?>Běžný účet
									<?php } elseif ($account->type == "savings") { ?>Spořící účet
									<?php } else { ?>Úvěrový účet<?php } ?>

								</a></td>
<?php } else { ?>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></td>
								<td>Spořící účet</td>
<?php } } elseif ($transactionType == 3) { ?>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
">
								<?php if ($account->type == "regular") { ?>Běžný účet
								<?php } elseif ($account->type == "savings") { ?>Spořící účet
								<?php } else { ?>Úvěrový účet<?php } ?>

							</a></td>
<?php } ?>
					</tr>
<?php $iterations++; } ?>
			</tbody>
		</table>
<?php } ?>

<?php if (isset($sharedAccounts)) { ?>
		<br>
		<h3>Sdílené účty</h3>

		<table class="table table-hover user_detail account_access">
			<thead>
				<tr>
					<th>Číslo účtu</th>
					<th>Název účtu</th>
					<th>Zůstatek</th>
					<th>Typ účtu</th>
				</tr>
			</thead>
			<tbody>
<?php $iterations = 0; foreach ($sharedAccounts as $account) { ?>
					<tr>
<?php if ($transactionType == 1) { if ($account->type != "savings") { ?>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionToClient", array($account->id, $clientID)), ENT_COMPAT) ?>
">
									<?php if ($account->type == "regular") { ?>Běžný účet
									<?php } else { ?>Úvěrový účet<?php } ?>

								</a></td>
<?php } else { ?>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></td>
								<td>Spořící účet</td>
<?php } } elseif ($transactionType == 2) { if ($account->type != "savings") { ?>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
								<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGetMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
">
									<?php if ($account->type == "regular") { ?>Běžný účet
									<?php } elseif ($account->type == "savings") { ?>Spořící účet
									<?php } else { ?>Úvěrový účet<?php } ?>

								</a></td>
<?php } else { ?>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></td>
								<td><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></td>
								<td>Spořící účet</td>
<?php } } elseif ($transactionType == 3) { ?>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->id, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->name, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($account->balance, ENT_NOQUOTES) ?></a></td>
							<td><a class="table-row search-row" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Admin:newTransactionGiveMoney", array($account->id, $clientID)), ENT_COMPAT) ?>
">
								<?php if ($account->type == "regular") { ?>Běžný účet
								<?php } elseif ($account->type == "savings") { ?>Spořící účet
								<?php } else { ?>Úvěrový účet<?php } ?>

							</a></td>
<?php } ?>
					</tr>
<?php $iterations++; } ?>
			</tbody>
		</table>
<?php } 
}}

//
// end of blocks
//

// template extending

$_l->extends = '../../../../@layout.latte'; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
// ?>


<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}